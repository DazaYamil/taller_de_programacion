programa MasterSlave
areas
  areaCuadrante: AreaPC(2,2,20,20)
  areaJefe: AreaP(1,1,1,1)
  area1: AreaP(5,1,5,1)
  area2: AreaP(8,1,8,1)
robots
	robot jefe
	variables 
		idRobot: numero
		av,ca:numero
	comenzar
		EnviarMensaje(1, t1)
		EnviarMensaje(2, t2)
		Repetir 10
			Random(idRobot, 1, 2)
			Random(av, 2,20)
			Random(ca, 2,20)
			si(idRobot = 1)
				EnviarMensaje(V, r1)
				EnviarMensaje(av, r1)
				EnviarMensaje(ca, r1)
			sino
				EnviarMensaje(V, r1)
				EnviarMensaje(av, r1)
				EnviarMensaje(ca, r1)
		
	fin

	robot trabajador
	variables 
		id: numero
	comenzar
		RecibirMensaje(id, jefe)
		derecha
	fin
variables
	t1: trabajador
	t2: trabajador
	jefe: jefe
comenzar
	AsignarArea(jefe, areaJefe)
	AsignarArea(t1, area1)
	AsignarArea(t1, area2)
	
	AsignarArea(t1, areaCuadrante)
	AsignarArea(t2, areaCuadrante)

	Iniciar(jefe, 1,1)
	Iniciar(t1, 5,1)
	Iniciar(t2, 8,1)
fin
programa productoConsumidor
procesos
  proceso DepositarPapeles
  comenzar
    repetir 5
      depositarPapel
  fin

  proceso IrADepositar(E av:numero; E ca:numero)
  comenzar
    BloquearEsquina(20,20)
    Pos(20,20)
    DepositarPapeles
    Pos(av,ca)
    LiberarEsquina(20,20)
  fin

  proceso JuntarFloresYVerificar(ES cantFlores:numero)
  variables
    avActual, caActual: numero
  comenzar
    avActual:= PosAv
    caActual:= PosCa
    mientras(HayPapelEnLaEsquina)
      tomarPapel
      cantFlores:= cantFlores + 1

      si(cantFlores = 5)
        IrADepositar(avActual,caActual)
        cantFlores:= 0
  fin

  proceso IrVerificarYjuntar(ES cantX: numero; E avActual:numero; E caActual:numero)
  variables 
    contador: numero
  comenzar
    contador:= 0
    BloquearEsquina(20,20) 
    Pos(20,20)
    mientras((HayPapelEnLaEsquina) & (contador < cantX))
      tomarPapel
      contador:=contador + 1
    si(contador <> cantX)
      repetir contador
        depositarPapel
      cantX:=0

    Pos(avActual,caActual)
    LiberarEsquina(20,20)
  fin
areas
  areaJuntarDepositar: AreaC(20,20,21,20)
  area1: AreaP(5,1,5,20)
  area2: AreaP(10,1,10,20)
  area3: AreaP(13,1,13,1)
  area4: AreaP(15,1,15,1)
robots
  robot productor
  variables
    cantFlores: numero 
  comenzar
    cantFlores:= 0
    mientras(PosCa < 20)
      JuntarFloresYVerificar(cantFlores)
      mover
    JuntarFloresYVerificar(cantFlores)
    
  fin

  robot consumidor 
  variables
    cantX: numero
    avActual,caActual: numero
  comenzar
    avActual:= PosAv
    caActual:= PosCa
    repetir 8
      Random(cantX, 2, 5)
      IrVerificarYjuntar(cantX, avActual, caActual)
      si(cantX > 0)
        repetir cantX
          depositarPapel
  fin

variables
  p1:productor
  p2:productor
  c1:consumidor
  c2:consumidor
comenzar
  AsignarArea(p1, area1)
  AsignarArea(p2, area2)
  AsignarArea(c1, area3)
  AsignarArea(c2, area4)

  AsignarArea(p1, areaJuntarDepositar)
  AsignarArea(p2, areaJuntarDepositar)
  AsignarArea(c1, areaJuntarDepositar)
  AsignarArea(c2, areaJuntarDepositar)
  
  Iniciar(p1, 5,1)
  Iniciar(p2, 10,1)
  Iniciar(c1, 13,1)
  Iniciar(c2, 15,1)
fin
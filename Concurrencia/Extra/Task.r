- 3 Robots tienen 8 intentos en TOTAL:
  - Juntar todas las flores dentro del cuadrante 40,40,60,60
  - En cada intento un robot fiscalizador eligira un robot aleatorio.
  - El robot fiscalizador debe eligir una esquina aleatorio para dicho Robot
  - Al completar los 8 intentos, deben depositar todas las flores en la esq 10,10
  - El robot fiscalizador la cantidad de flores juntadas por los Robots


programa task1
procesos
	proceso IrYJuntarFlores(ES cantFlores:numero; E av: numero; E ca:numero; E avActual:numero; E caActual:numero )
	comenzar
		BloquarEsquina(av, ca)
		Pos(av, ca)
		mientras(HayFlorEnLaEsquina)
			tomarFlor
			cantFlores:=cantFlores + 1
		Pos(avActual, caActual)
		LiberarEsquina(av, ca)
	fin
areas
  area1: AreaP(1,1,1,1)
  area2: AreaP(2,1,2,1)
  area3: AreaP(3,1,3,1)
  area4: AreaP(7,1,7,1)
  areaJuntar: AreaP(5,5,20,20)
  areaDepositar : AreaC(20,2,21,2)
robots
  robot fiscalizador
  variables
		av,ca:numero
	comenzar
		EnviarMensaje(1, r1)
		EnviarMensaje(2, r2)
		EnviarMensaje(3, r3)

		Random(av, 5,20)
		Random(ca, 5,20)
		EnviarMensaje
  fin

  robot florero
  variables
		avActual, caActual: numero
		cantFlores: numero
		id: numero
	comenzar
		cantFlores:= 0
		avActual:= PosAv
		caActual:= PosCa
    
		RecibirMensaje(id, rf)

		repetir 8
			RecibirMensaje(av, rf)
			RecibirMensaje(ca, rf)
			IrYJuntarFlores(cantFlores,av,ca,avActual,caActual)
			
  fin
variables
  r1:florero
  r2:florero
  r3:florero
  rf:fiscalizador
comenzar
  AsignarArea(r1, area1)
  AsignarArea(r2, area2)
  AsignarArea(r3, area3)
  AsignarArea(rf, area4)

  AsignarArea(r1, areaJuntar)
  AsignarArea(r2, areaJuntar)
  AsignarArea(r3, areaJuntar)

  AsignarArea(r1,areaDepositar)
  AsignarArea(r2,areaDepositar)
  AsignarArea(r3,areaDepositar)
  AsignarArea(rf,areaDepositar)

  Iniciar(r1, 1,1)
  Iniciar(r2, 2,1)
  Iniciar(r3, 3,1)
  Iniciar(rf, 7,1)
fin
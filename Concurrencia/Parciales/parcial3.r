programa parcialThree
procesos
	proceso JuntarFlor( E av:numero, E ca:numero )
	comenzar
		BloquearEsquina(50,50)
		Pos(50,50)
		TomarUnaFlor
		Pos(av,ca)
		LiberarEsquina(50,50)
	fin
areas
  areaJuntar: AreaP(50,50,50,50)
  areaJefe: AreaP(1,1,1,1)
  area1: AreaP(2,2,2,2)
  area2: AreaP(3,3,3,3)
  area3: AreaP(4,4,4,4)
  area4: AreaP(5,5,5,5)
robots
  robot limpiador
	variables
		id:numero
		avanzar:boolean
		av,ca:numero
	comenzar
		av:=PosAv
		ca:=PosCa

		RecibirMensaje(id, rj)
		RecibirMensaje(avanzar, rj)
		if(avanzar)
			JuntarFlor(av,ca)
	fin

	robot jefe
	variables
		id:numero
	comenzar
		EnviarMensaje(1,r1)
		EnviarMensaje(2,r2)
		EnviarMensaje(3,r3)
		EnviarMensaje(4,r4)

		repetir 5
			Random(id,1,4)
			si(id = 1)
				EnviarMensaje(V,r1)
				EnviarMensaje(F,r2)
				EnviarMensaje(F,r3)
				EnviarMensaje(F,r4)
			sino
				si(id = 2)
					EnviarMensaje(F,r1)
					EnviarMensaje(V,r2)
					EnviarMensaje(F,r3)
					EnviarMensaje(F,r4)
				sino
					si(id = 3)
						EnviarMensaje(F,r1)
						EnviarMensaje(F,r2)
						EnviarMensaje(V,r3)
						EnviarMensaje(F,r4)
					sino
						si(id = 4)
							EnviarMensaje(F,r1)
							EnviarMensaje(F,r2)
							EnviarMensaje(F,r3)
							EnviarMensaje(V,r4)
	fin
variables
	r1:limpiador
	r2:limpiador
	r3:limpiador
	r4:limpiador
	rj:jefe
comenzar
	AsignarArea(r1,area1)
	AsignarArea(r2,area2)
	AsignarArea(r3,area3)
	AsignarArea(r4,area4)
	AsignarArea(rf,areaJefe)

	AsignarArea(r1,areaJuntar)
	AsignarArea(r2,areaJuntar)
	AsignarArea(r3,areaJuntar)
	AsignarArea(r4,areaJuntar)

	Iniciar(r1,2,2)
	Iniciar(r2,3,3)
	Iniciar(r3,4,4)
	Iniciar(r4,5,5)
	Iniciar(rj,1,1)
fin

programa parcial4
procesos 
  proceso JuntarFlores(ES cantFlores:numero)
  comenzar
    mientras(HayFlorEnLaEsquina)
      tomarFlor
      cantFlores:=cantFlores + 1
  fin

  proceso HacerLado(ES cantFlores:numero)
  comenzar
    repetir 5
      JuntarFlores(cantFlores)
      mover
    derecha
  fin
areas
  areaDepositar : AreaC(10,10,10,10)
  areaCoordinador : AreaP(20,1,20,1)
  area1 : AreaP(1,1,6,6)
  area2 : AreaP(7,1,12,6)
  area3 : AreaP(13,1,18,6)

robots
  robot trabajador
  variables
    cantFlores:numero
    id:numero
    seguir:boolean
  comenzar
    RecibirMensaje(id, rc)
    
    repetir 4
      HacerLado(cantFlores)
      EnviarMensaje(id, rc)
      RecibirMensaje(seguir, rc)

    EnviarMensaje(id, rc)
		EnviarMensaje(cantFlores, rc)

  fin

  robot coordinador
  variables
    idRobot:numero
		cantidadFlores:numero
		maximoFlores,robotGanador:numero
  comenzar
    EnviarMensaje(1, r1)
    EnviarMensaje(2, r2)
    EnviarMensaje(3, r3)

		maximoFlores = 0

    repetir 4
      repetir 3
        RecibirMensaje(idRobot, *)
      EnviarMensaje(V, r1)
      EnviarMensaje(V, r2)
      EnviarMensaje(V, r3)

		repetir 3
			RecibirMensaje(idRobot, *)
			si(idRobot = 1)
				RecibirMensaje(cantidadFlores, r1)
			sino
				si(idRobot = 2)
					RecibirMensaje(cantidadFlores, r2)
				sino
					RecibirMensaje(cantidadFlores, r3)
			
			si(cantidadFlores > maximoFlores)
				maximoFlores = cantidadFlores
				robotGanador = idRobot

			Informar('ROBOT-GANADOR', robotGanador)
			Informar('CANTIDAD-FLORES-JUNTADAS', maximoFlores)

  fin

variables
  r1:trabajador
  r2:trabajador
  r3:trabajador
  rc:coordinador
comenzar
  AsignarArea(r1, area1)
  AsignarArea(r2, area2)
  AsignarArea(r3, area3)
  AsignarArea(rc, areaCoordinador)

  AsignarArea(r1, areaDepositar)
  AsignarArea(r2, areaDepositar)
  AsignarArea(r3, areaDepositar)

  Iniciar(r1, 1,1)
  Iniciar(r2, 7,1)
  Iniciar(r3, 13,1)
  Iniciar(rc, 20,1)

fin

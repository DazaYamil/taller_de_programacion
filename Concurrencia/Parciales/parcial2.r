programa parcial
procesos
	proceso Izquierda
	comenzar
		repetir 3 
			derecha
	fin
areas
	areaDepositar: AreaPC(20,20,20,20)

	area1Izquierdo: AreaP(9,19,19,19)
	area2Izquierdo: AreaC(9,20,19,20)
	area3Izquierdo: AreaP(9,21,19,21)

	area1Derecho: AreaP(21,19,31,19)
  area2Derecho: AreaC(21,20,31,20)
  area3Derecho: AreaP(21,21,31,21)

	areaFiscalizador: AreaP(20,18,20,18)
robots
  robot equipoAzul
	comenzar
		derecha
		repetir 10
			mover
	fin

	robot equipoVerde
	comenzar
		Izquierda
		repetir 10
			mover
	fin

	robot fiscalizador
	comenzar
		derecha
	fin
variables
	ra1:equipoAzul
	ra2:equipoAzul
	ra3:equipoAzul

	rv1:equipoVerde
	rv2:equipoVerde
	rv3:equipoVerde

	rf:fiscalizador
comenzar
	AsignarArea(ra1,area1Izquierdo)
	AsignarArea(ra2,area2Izquierdo)
	AsignarArea(ra3,area3Izquierdo)

	AsignarArea(rv1,area1Derecho)
	AsignarArea(rv2,area2Derecho)
	AsignarArea(rv3,area3Derecho)

	AsignarArea(ra2,areaDepositar)
	AsignarArea(rv2,areaDepositar)

	AsignarArea(ra1,area2Izquierdo)
	AsignarArea(ra3,area2Izquierdo)

	AsignarArea(rv1,area2Derecho)
	AsignarArea(rv3,area2Derecho)

	AsignarArea(rf, areaFiscalizador)

	Iniciar(ra1,9,19)
	Iniciar(ra2,9,20)
	Iniciar(ra1,9,21)

	Iniciar(rv1,31,19)
	Iniciar(rv2,31,20)
	Iniciar(rv1,31,21)

	Iniciar(rf, 20,18)
fin
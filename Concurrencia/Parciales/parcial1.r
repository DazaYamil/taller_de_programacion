programa parcial
procesos
  proceso JuntarFlores( ES cantF:numero )
  comenzar
    mientras(HayFlorEnLaEsquina)
      tomarFlor
      cantF:=cantF + 1
  fin

  proceso HacerLado(ES cantF:numero)
  comenzar
    repetir 5
      JuntarFlores(cantF)
      mover
    derecha
  fin
  
  proceso DepositarFlores(E cantF:numero)
  variables
    av,ca:numero
  comenzar
    av:=PosAv
    ca:=PosCa
    repetir cantF
      BloquearEsquina(10,10)
      Pos(10,10)
      depositarFlor
      Pos(av,ca)
      LiberarEsquina(10,10)
  fin

areas
  area1: AreaP(1,1,6,6)
  area2: AreaP(7,1,12,6)
  area3: AreaP(13,1,18,6)
  areaCoordinador: AreaP(20,1,20,1)
  areaDepositar: AreaPC(10,10,10,10)
robots
  robot trabajador
  variables
    id:numero
    cantF:numero
    seguir:boolean
    noGane:boolean
  comenzar
    RecibirMensaje(id,rc)

    cantF:=0
    repetir 4
      HacerLado(cantF)
      EnviarMensaje(V, rc)
      RecibirMensaje(seguir, rc)

    EnviarMensaje(id, rc)
    EnviarMensaje(cantF, rc)

    RecibirMensaje(noGane,rc)
    si(noGane)
      DepositarFlores(cantF)
    sino
      Informar('YO-GANE',id)
  fin

  robot coordinador
  variables
    termino:boolean
    idRobot:numero
    cantFlores:numero
    max,idGanador:numero
    perdedor1,perdedor2:numero
  comenzar
    max:=0

    EnviarMensaje(1,r1)
    EnviarMensaje(2,r2)
    EnviarMensaje(3,r3)

    repetir 4
      repetir 3
        RecibirMensaje(termino, *)

      EnviarMensaje(V,r1)
      EnviarMensaje(V,r2)
      EnviarMensaje(V,r3)

    repetir 3
      RecibirMensaje(idRobot, *)
      si(idRobot = 1)
        RecibirMensaje(cantFlores, r1)
      sino
        si(idRobot = 2)
          RecibirMensaje(cantFlores, r2)
        sino
          si(idRobot = 3)
            RecibirMensaje(cantFlores, r3)
      si(cantFlores > max)
        max:=cantFlores
        idGanador:=idRobot

    si(max > 0)
      si(idGanador = 1)
        EnviarMensaje(F,r1)    
        EnviarMensaje(V,r2)    
        EnviarMensaje(V,r3)
      sino
        si(idGanador = 2)
          EnviarMensaje(V,r1)    
          EnviarMensaje(F,r2)    
          EnviarMensaje(V,r3)
        sino
          EnviarMensaje(V,r1)    
          EnviarMensaje(V,r2)    
          EnviarMensaje(F,r3)

      Informar('Robot-Ganador',idGanador)
    sino
      Informar('NO-HAY-GANADOR-TODOS-JUNTARON-CANT-FLORES',max)    
  fin

variables
  r1:trabajador
  r2:trabajador
  r3:trabajador
  rc:coordinador
comenzar
  AsignarArea(r1, area1)
  AsignarArea(r2, area2)
  AsignarArea(r3, area3)
  AsignarArea(rc, areaCoordinador)

  AsignarArea(r1, areaDepositar)
  AsignarArea(r2, areaDepositar)
  AsignarArea(r3, areaDepositar)

  Iniciar(r1,1,1)
  Iniciar(r2,7,1)
  Iniciar(r3,13,1)
  Iniciar(rc,20,1)
fin
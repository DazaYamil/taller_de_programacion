programa exerciseTwo
procesos
  proceso DepositarPapeles(ES cantP:numero)
  variables
    av,ca:numero
  comenzar
    av:=PosAv
    ca:=PosCa
    BloquearEsquina(15,5)
    Pos(15,5)
    repetir 5
      depositarPapel
      cantP:=cantP - 1
    Pos(av,ca)
    LiberarEsquina(15,5)
  fin

  proceso JuntarPapeles(ES cantP:numero)
  comenzar
    mientras(HayPapelEnLaEsquina)
      tomarPapel
      cantP:=cantP + 1
      si(cantP = 5)
        DepositarPapeles(cantP)
  fin 

  proceso VerificarYJuntar(E xFlores:numero)
  variables
    cont:numero
  comenzar
    cont:=0
    mientras((HayPapelEnLaEsquina) & (cont < xFlores))
      tomarPapel
      cont:=cont + 1
    si(cont <> xFlores)
      repetir cont 
        depositarPapel
  fin
areas
  areaDepositar: AreaP(15,5,15,5)
  area1: AreaP(5,1,5,5)
  area2: AreaP(10,1,10,5)
  area3: AreaP(12,1,12,1)
  area4: AreaP(14,1,14,1)

robots
  robot productor
  variables
    cantP:numero
  comenzar
    cantP:=0
    mientras(PosCa < 5)
      JuntarPapeles(cantP)
      mover
    JuntarPapeles(cantP)
  fin
  
  robot consumidor
  variables
    xFlores:numero 
    av,ca:numero
  comenzar
    av:=PosAv
    ca:=PosCa
    repetir 5
      Random(xFlores, 2, 5)
      Informar('QUIERO-X-FLORES',xFlores)
      BloquearEsquina(15,5)
      Pos(15,5)
      VerificarYJuntar(xFlores)
      Pos(av,ca)
      LiberarEsquina(15,5)
  fin

variables
  rp1:productor
  rp2:productor
  rc1:consumidor
  rc2:consumidor
comenzar
  {Area asignada para depositar}
  AsignarArea(rp1, areaDepositar)
  AsignarArea(rp2, areaDepositar)
  AsignarArea(rc1, areaDepositar)
  AsignarArea(rc2, areaDepositar)
  
  {Area personal de cada productor}
  AsignarArea(rp1, area1)
  AsignarArea(rp2, area2)
  AsignarArea(rc1, area3)
  AsignarArea(rc2, area4)

  {Iniciamos...}
  Iniciar(rp1,5,1)
  Iniciar(rp2,10,1)
  Iniciar(rc1,12,1)
  Iniciar(rc2,14,1)
fin
programa exerciseOne
procesos
  proceso DepositarFlores( E cantFlores:numero )
  comenzar
    repetir cantFlores
      depositarFlor
  fin
  proceso JuntarFlores( ES flores:numero )
  comenzar
    flores:=0
    mientras(HayFlorEnLaEsquina)
      tomarFlor
      flores:=flores + 1
  fin
areas
  area1: AreaC(1,1,1,10)
  area2: AreaC(2,1,2,10)
  area3: AreaC(3,1,3,10)
  areaServidor: AreaP(5,1,5,1)
{robots ....................... }
robots
  robot clientes
  variables
    id:numero
    cantFlores,flores:numero
    av,ca:numero
    avanzar:boolean
  comenzar
    av:=PosAv
    ca:=PosCa
    
    {ID enviado por el Robot Servidor}
    RecibirMensaje(id, rs)
    
    Random(cantFlores,1,4)
    {Enviar cantidad de flores random y avenida y calle actual}
    EnviarMensaje(id, rs)
    EnviarMensaje(cantFlores, rs)
    EnviarMensaje(av, rs)
    EnviarMensaje(ca + 1, rs)
    RecibirMensaje(avanzar, rs)
    si(avanzar)
      mover
      JuntarFlores(flores)
      Pos(av,ca)
      repetir flores
        mover
        depositarFlor

  fin

  robot servidor
  variables
    idRobot:numero
    cantFlores:numero
    av,ca:numero
  comenzar
    {Enviar ID a cada robot}
    EnviarMensaje(1,r1)
    EnviarMensaje(2,r2)
    EnviarMensaje(3,r3)

    repetir 3
      RecibirMensaje(idRobot, *)
      si(idRobot = 1)
        RecibirMensaje(cantFlores, r1)
        RecibirMensaje(av, r1)
        RecibirMensaje(ca, r1)
      sino
        si(idRobot = 2)
          RecibirMensaje(cantFlores, r2)
          RecibirMensaje(av, r2)
          RecibirMensaje(ca, r2)
        sino
          si(idRobot = 3)
            RecibirMensaje(cantFlores, r3)
            RecibirMensaje(av, r3)
            RecibirMensaje(ca, r3)
            
      BloquearEsquina(av,ca)
      Pos(av,ca)
      DepositarFlores(cantFlores)
      Pos(5,1)
      LiberarEsquina(av,ca)
      
      {Enviar el OK cuando ya deposito las flores}
      si(idRobot = 1)
        EnviarMensaje(V,r1)
        EnviarMensaje(F,r2)
        EnviarMensaje(F,r3)
      sino
        si(idRobot = 2)
          EnviarMensaje(V,r2)
          EnviarMensaje(F,r3)
          EnviarMensaje(F,r1)
        sino
          si(idRobot = 3)
            EnviarMensaje(V,r3)
            EnviarMensaje(F,r2)
            EnviarMensaje(F,r1)
  fin

{variables .................... }
variables
  r1:clientes
  r2:clientes
  r3:clientes
  rs:servidor
comenzar
  AsignarArea(r1,area1)
  AsignarArea(r2,area2)
  AsignarArea(r3,area3)

  AsignarArea(rs,area1)
  AsignarArea(rs,area2)
  AsignarArea(rs,area3)

  AsignarArea(rs,areaServidor)

  Iniciar(r1,1,1)
  Iniciar(r2,2,1)
  Iniciar(r3,3,1)
  Iniciar(rs,5,1)
fin
1) Realice un programa para que un robot junte todas las flores de la avenida 1 y las deposite al final de dicha avenida. Al finalizar, debe informar la cantidad de flores depositadas y la cantidad de esquinas sin flores que encontró durante el recorrido.

programa Ejercicio1
areas
  ciudad1 : AreaC(1,1,10,10)
robots 
  robot tipo1
  variables
    contador: numero
  comenzar
		repetir 3
			mientras(PosCa < 10)
				si(HayFlorEnLaEsquina)
					mientras(HayFlorEnLaEsquina)
						tomarFlor
				sino
					contador := contador + 1
				mover
			mientras(HayFlorEnLaBolsa)
				depositarFlor
			Informar(contador)
			Pos(PosAv + 2, 1)
  fin
variables  
  r1: tipo1
comenzar 
  AsignarArea(r1,ciudad1)
  Iniciar(r1, 1, 1)
fin
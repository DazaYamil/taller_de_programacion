programa Ejercicio2
areas 
  ciudad1 : AreaC(5,1,9,5)
robots
  robot tipo1
  variables
    aux: numero
  comenzar
    aux := 0
    si(PosAv = 5)&(PosCa = 5)
      derecha
    sino 
      si (PosAv = 9)&(PosCa = 5)
        repetir 2
          derecha
      sino 
        si (PosAv = 9)&(PosCa = 1)
          repetir 3
            derecha
    repetir 3
      mientras(HayPapelEnLaEsquina)
        tomarPapel
        aux := aux + 1
      mover
    Informar(HayPapelEnLaBolsa)
  fin
variables
  r1: tipo1
  r2: tipo1
  r3: tipo1
  r4: tipo1
comenzar
  AsignarArea(r1,ciudad1)
  AsignarArea(r2,ciudad1)
  AsignarArea(r3,ciudad1)
  AsignarArea(r4,ciudad1)
  Iniciar(r1, 5, 1)
  Iniciar(r2, 5, 5)
  Iniciar(r3, 9, 5)
  Iniciar(r4, 9, 1)
fin

programa exerciseFive
procesos
  proceso JuntarUnPapel(ES seguir:boolean)
  comenzar
    si(HayPapelEnLaEsquina)
      tomarPapel
    sino
      seguir:=F
  fin
  proceso DepositarPapel
  comenzar
    si(HayPapelEnLaBolsa)
      depositarPapel
  fin
  
areas
  area1: AreaP(4,1,4,10)
  area2: AreaP(6,1,6,10)
  area3: AreaP(8,1,8,10)
  area4: AreaP(10,1,10,10)
  areaPapeles: AreaP(11,11,11,11)
  areaC: AreaP(1,1,1,1)
  
robots
  robot competidor
  variables
    id:numero
    seguir:boolean
    av,ca,cantPasos:numero
  comenzar
    RecibirMensaje(id,rc)
    
    cantPasos:=0
    seguir:=V
    
    av:=PosAv
    ca:=PosCa
    
    mientras(seguir)
      BloquearEsquina(11,11)
      Pos(11,11)
      JuntarUnPapel(seguir)
      Pos(av,ca)
      LiberarEsquina(11,11)
      DepositarPapel
      si((seguir) & (ca < 10))
        mover
        cantPasos:=cantPasos + 1
      sino
        seguir:=F
      ca:=ca + 1
    EnviarMensaje(id,rc)
    EnviarMensaje(cantPasos,rc)
  fin

  robot coordinar
  variables
    idRobot,robotGanador:numero
    max,aux:numero
  comenzar
    robotGanador:=0
    max:=0
    aux:=0

    EnviarMensaje(1,r1)
    EnviarMensaje(2,r2)
    EnviarMensaje(3,r3)
    EnviarMensaje(4,r4)

    repetir 4
      RecibirMensaje(idRobot, *)
      si(idRobot = 1)
        RecibirMensaje(aux,r1)
      sino
        si(idRobot = 2)
          RecibirMensaje(aux, r2)
        sino
          si(idRobot = 3)
            RecibirMensaje(aux, r3)
          sino
            si(idRobot = 4)
              RecibirMensaje(aux, r4)
      si(aux > max)
        max:=aux
        robotGanador:=idRobot
    Informar('Robot-Ganador',robotGanador)
    Informar('PasosRecorridos',max)
  fin
  
variables
  r1:competidor
  r2:competidor
  r3:competidor
  r4:competidor
  rc:coordinar
comenzar
  AsignarArea(r1,area1)
  AsignarArea(r2,area2)
  AsignarArea(r3,area3)
  AsignarArea(r4,area4)
  
  AsignarArea(r1,areaPapeles)
  AsignarArea(r2,areaPapeles)
  AsignarArea(r3,areaPapeles)
  AsignarArea(r4,areaPapeles)
  
  AsignarArea(rc,areaC)
  
  Iniciar(r1,4,1)
  Iniciar(r2,6,1)
  Iniciar(r3,8,1)
  Iniciar(r4,10,1)
  Iniciar(rc,1,1)
fin
programa exerciseOne
procesos
  proceso JuntarFlores
  comenzar
    mientras(HayFlorEnLaEsquina)
      tomarFlor
  fin
  proceso JuntarPapeles
  comenzar
    mientras(HayPapelEnLaEsquina)
      tomarPapel
  fin
  proceso DepositarElementos
  comenzar
    si(HayFlorEnLaBolsa)
      mientras(HayFlorEnLaBolsa)
        depositarFlor
    sino
      mientras(HayPapelEnLaBolsa)
        depositarPapel
  fin
areas
  AreaFloreros: AreaPC(1,1,5,10)
  AreaPapeleros: AreaPC(6,1,10,9)
  Area1: AreaPC(6,10,7,10)
  Area2: AreaPC(8,10,9,10)
  AreaDepositar: AreaPC(10,10,10,10)
robots
  robot Floreros
  variables
    avInicial,caInicial:numero
    av,ca:numero
  comenzar
    avInicial:=PosAv
    caInicial:=PosCa
    repetir 5
      Random(av,1,5)
      Random(ca,1,10)
      BloquearEsquina(av,ca)
      Pos(av,ca)
      JuntarFlores
      Pos(avInicial,caInicial)
      LiberarEsquina(av,ca)
    derecha
    BloquearEsquina(10,10)
    Pos(10,10)
    DepositarElementos
    Pos(avInicial,caInicial)
    LiberarEsquina(10,10)
  fin

  robot Papeleros
  variables
    avInicial,caInicial:numero
    av,ca:numero
  comenzar
    avInicial:=PosAv
    caInicial:=PosCa
    repetir 3
      Random(av,6,10)
      Random(ca,1,9)
      BloquearEsquina(av,ca)
      Pos(av,ca)
      JuntarPapeles
      Pos(avInicial,caInicial)
      LiberarEsquina(av,ca)
    derecha
    BloquearEsquina(10,10)
    Pos(10,10)
    DepositarElementos
    Pos(avInicial,caInicial)
    LiberarEsquina(10,10)
  fin
variables
  f1:Floreros
  f2:Floreros
  p1:Papeleros
  p2:Papeleros
comenzar
  AsignarArea(f1,AreaFloreros)
  AsignarArea(f2,AreaFloreros)
  AsignarArea(p1,AreaPapeleros)
  AsignarArea(p2,AreaPapeleros)
  
  AsignarArea(f1,Area1)
  AsignarArea(f2,Area1)
  AsignarArea(p1,Area2)
  AsignarArea(p2,Area2)

  AsignarArea(f1,AreaDepositar)
  AsignarArea(f2,AreaDepositar)
  AsignarArea(p1,AreaDepositar)
  AsignarArea(p2,AreaDepositar)

  Iniciar(f1,6,10)
  Iniciar(f2,7,10)
  Iniciar(p1,8,10)
  Iniciar(p2,9,10)
fin
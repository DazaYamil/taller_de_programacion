programa exerciseThree
procesos
  proceso juntarUnaFlor(ES cantF:numero; ES seguir:boolean)
  comenzar
    si(HayFlorEnLaEsquina)
      tomarFlor
      cantF:=cantF + 1
    sino  
      seguir:=F
  fin
areas
  areaJefe: AreaP(1,1,1,1)
  area1: AreaP(2,1,2,1)
  area2: AreaP(3,1,3,1)
  area3: AreaP(4,1,4,1)
  area4: AreaP(5,1,5,1)
  limpiarArea: AreaC(2,2,10,10)
robots
  robot floreros
  variables
    id:numero
    avInicial, caInicial:numero
    av,ca:numero
    cantF:numero
    seguir:boolean
  comenzar
    {Recibo mi ID del robot jefe}
    RecibirMensaje(id,rj)
    
    avInicial:=PosAv
    caInicial:=PosCa

    RecibirMensaje(av,rj)
    RecibirMensaje(ca,rj)

    cantF:=0
    seguir:=V
    mientras(seguir)
      BloquearEsquina(av,ca)
      Pos(av,ca)
      juntarUnaFlor(cantF,seguir)
      Pos(avInicial,caInicial)
      LiberarEsquina(av,ca)
    EnviarMensaje(id,rj)
    EnviarMensaje(cantF,rj)

  fin

  robot jefe
  variables
    av,ca:numero
    idRobot:numero
    idGanador,max,cantF:numero
  comenzar
    max:=0{Inicializo la variable auxiliar maxima}
    {Envio ID a cada Robot}
    EnviarMensaje(1,r1)
    EnviarMensaje(2,r2)
    EnviarMensaje(3,r3)
    EnviarMensaje(4,r4)

    {Avenida y calle aleatoria}
    Random(av,2,10)
    Random(ca,2,10)
    Informar('Avenida-Aleatoria',av)
    Informar('Calle-Aleatorio',ca)
    
    EnviarMensaje(av,r1)
    EnviarMensaje(ca,r1)
    EnviarMensaje(av,r2)
    EnviarMensaje(ca,r2)
    EnviarMensaje(av,r3)
    EnviarMensaje(ca,r3)
    EnviarMensaje(av,r4)
    EnviarMensaje(ca,r4)

    {Recibir a los robots con la cantidad de flores}
    repetir 4
      RecibirMensaje(idRobot, *)
      si(idRobot = 1)
        RecibirMensaje(cantF,r1)
      sino
        si(idRobot = 2)
          RecibirMensaje(cantF, r2)
        sino
          si(idRobot = 3)
            RecibirMensaje(cantF, r3)
          sino
            si(idRobot = 4)
              RecibirMensaje(cantF, r4)
      si(cantF > max)
        max:=cantF
        idGanador:=idRobot
    si(max > 0)
      Informar('Robot-Ganador',idRobot)
      Informar('Cantidad-Flores',max)
    sino
      Informar('NoHuboGanadorTodosJuntaron',max)  
  fin
variables
  r1:floreros
  r2:floreros
  r3:floreros
  r4:floreros
  rj:jefe
comenzar
  AsignarArea(rj,areaJefe)
  AsignarArea(r1,area1)
  AsignarArea(r2,area2)
  AsignarArea(r3,area3)
  AsignarArea(r4,area4)

  AsignarArea(r1,limpiarArea)
  AsignarArea(r2,limpiarArea)
  AsignarArea(r3,limpiarArea)
  AsignarArea(r4,limpiarArea)

  Iniciar(rj,1,1)
  Iniciar(r1,2,1)
  Iniciar(r2,3,1)
  Iniciar(r3,4,1)
  Iniciar(r4,5,1)
fin
programa exerciseFour
procesos
  proceso TomarFlor(ES seguir:boolean)
  comenzar
    si(HayFlorEnLaEsquina)
      tomarFlor
    sino
      seguir:=F
  fin
  proceso DepositarFlor
  comenzar
    si(HayFlorEnLaBolsa)
      depositarFlor
  fin
  proceso JuntarFlores(ES cantF:numero)
  comenzar
    mientras(HayFlorEnLaEsquina)
      tomarFlor
      cantF:=cantF + 1
  fin
areas
  AreaFlores: AreaP(10,10,10,10)
  AreaDepositar: AreaP(11,11,11,11)
  Area1: AreaP(9,9,9,9)
  Area2: AreaP(9,10,9,10)
  Area3: AreaP(9,11,9,11)
  Area4: AreaP(9,12,9,12)
  Area5: AreaP(1,1,1,1)

robots
  robot recolectores
  variables
    id:numero
    avInicial,caInicial:numero
    seguir:boolean
  comenzar
    RecibirMensaje(id,rc)
    avInicial:=PosAv
    caInicial:=PosCa
    
    seguir:=V
    mientras(seguir)
      BloquearEsquina(10,10)
      Pos(10,10)
      TomarFlor(seguir)
      Pos(avInicial,caInicial)
      BloquearEsquina(11,11)
      LiberarEsquina(10,10)
      Pos(11,11)
      DepositarFlor
      Pos(avInicial,caInicial)
      LiberarEsquina(11,11)
    
    EnviarMensaje(id,rc)
     
  fin

  robot coordinador
  variables
    avInicial:numero
    caInicial:numero
    cantF:numero
    idRobot:numero
  comenzar
    avInicial:=PosAv
    caInicial:=PosCa
    
    EnviarMensaje(1,r1)
    EnviarMensaje(2,r2)
    EnviarMensaje(3,r3)
    EnviarMensaje(4,r4)
    derecha
    
    repetir 3
      RecibirMensaje(idRobot, *)
    
    RecibirMensaje(idRobot, *)
    Informar('Robot-Que-Termino-Ultimo',idRobot)
    Pos(11,11)
    JuntarFlores(cantF)
    Pos(avInicial,caInicial)
    Informar('Cantidad-de-Flores-Recolectadas',cantF)
  fin

variables
  r1:recolectores
  r2:recolectores
  r3:recolectores
  r4:recolectores
  rc:coordinador

comenzar
  {Asignacion de posicion inicial por robot}
  AsignarArea(r1,Area1)
  AsignarArea(r2,Area2)
  AsignarArea(r3,Area3)
  AsignarArea(r4,Area4)

  {Asignacion para Tomar una Flor}
  AsignarArea(r1,AreaFlores)
  AsignarArea(r2,AreaFlores)
  AsignarArea(r3,AreaFlores)
  AsignarArea(r4,AreaFlores)

  {Asignacion para Depositar Flor}
  AsignarArea(r1,AreaDepositar)
  AsignarArea(r2,AreaDepositar)
  AsignarArea(r3,AreaDepositar)
  AsignarArea(r4,AreaDepositar)

  {Asignacion para el robot recolector, Posicion y JuntarFlores}
  AsignarArea(rc,Area5)
  AsignarArea(rc,AreaDepositar)
  
  {Iniciar a todos los robots}
  Iniciar(r1,9,9)
  Iniciar(r2,9,10)
  Iniciar(r3,9,11)
  Iniciar(r4,9,12)
  Iniciar(rc,1,1)

fin
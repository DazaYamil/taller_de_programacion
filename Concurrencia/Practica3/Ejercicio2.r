programa exerciseTwo
procesos
  proceso juntarFlores(ES cantF:numero)
  comenzar
    mientras(HayFlorEnLaEsquina)
      tomarFlor
      cantF:=cantF + 1
  fin
  
  proceso juntarPapeles(ES cantP:numero)
  comenzar
    mientras(HayPapelEnLaEsquina)
      tomarPapel
      cantP:=cantP + 1
  fin
  
  proceso izquierda
  comenzar
    repetir 3
      derecha
  fin

  proceso hacerEscalonF(ES escalon:numero; ES cantF:numero)
  comenzar
    repetir escalon
      juntarFlores(cantF)
      mover
    derecha
    repetir escalon
      juntarFlores(cantF)
      mover
    izquierda
  fin

  proceso hacerEscalonP(ES escalon:numero; ES cantP:numero)
  comenzar
    repetir escalon
      juntarPapeles(cantP)
      mover
    izquierda
    repetir escalon
      juntarPapeles(cantP)
      mover
    derecha
  fin

  proceso ultimoEscalonF(ES cantF:numero; E id:numero)
  comenzar
    juntarFlores(cantF)
    mover
    derecha
    BloquearEsquina((PosAv + 1), PosCa)
    juntarFlores(cantF)
    mover
    EnviarMensaje(id,rj)
    derecha
    derecha
    mover
    LiberarEsquina((PosAv + 1), PosCa)
  fin

  proceso ultimoEscalonP(ES cantP:numero; E id:numero)
  comenzar
    juntarPapeles(cantP)
    mover
    izquierda
    BloquearEsquina((PosAv - 1), PosCa)
    juntarPapeles(cantP)
    mover
    EnviarMensaje(id,rj)
    derecha
    derecha
    mover
    LiberarEsquina((PosAv - 1), PosCa)
  fin
areas
  areaJefe: AreaP(15,1,15,1)
  areaCarrera: AreaC(1,1,100,100)
robots
  robot competidorFlorero
  variables
    escalon:numero
    cantF:numero
    id:numero
  comenzar
    RecibirMensaje(id,rj)
    cantF:=0
    escalon:=5
    repetir 4
      hacerEscalonF(escalon,cantF)
      escalon:=escalon - 1
    ultimoEscalonF(cantF,id)
    
    escalon:=escalon + 1
    repetir 4
      hacerEscalonF(escalon,cantF)
      escalon:=escalon + 1
    derecha
    Informar('ROBOT-FLORERO',cantF)
  fin
  robot competidorPapelero
  variables
    escalon:numero
    cantP:numero
    id:numero
  comenzar
    RecibirMensaje(id,rj)
    cantP:=0
    escalon:=5
    repetir 4
      hacerEscalonP(escalon,cantP)
      escalon:=escalon - 1
    ultimoEscalonP(cantP,id)
    
    escalon:=escalon + 1
    repetir 4
      hacerEscalonP(escalon,cantP)
      escalon:=escalon + 1
    izquierda
    Informar('ROBOT-PAPELERO',cantP)
  fin

  robot jefe
  variables
    idRobot:numero
  comenzar
    EnviarMensaje(2,r2)
    EnviarMensaje(1,r1)
    RecibirMensaje(idRobot, *)
    si(idRobot = 1)
      Informar('Primer-Puesto',idRobot)
      Informar('Segundo-Puesto',(idRobot + 1))
    sino
      Informar('Primer-Puesto',idRobot)
      Informar('Segundo-Puesto',(idRobot - 1))
  fin
variables
  r1:competidorFlorero
  r2:competidorPapelero
  rj:jefe
comenzar
  AsignarArea(r1,areaCarrera)
  AsignarArea(r2,areaCarrera)
  AsignarArea(rj,areaJefe)
  Iniciar(r1,1,1)
  Iniciar(r2,31,1)
  Iniciar(rj,15,1)
fin
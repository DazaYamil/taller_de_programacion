programa exerciseTwo
procesos
  proceso Izquierda
  comenzar
    repetir 3
      derecha
  fin
  
  proceso Inicializar(ES cantF:numero; ES cantP:numero)
  comenzar
    cantF:=0
    cantP:=0
  fin
  
  proceso JuntarEsquina(ES cantF:numero; ES cantP:numero)
  comenzar
    mientras(HayFlorEnLaEsquina)
      tomarFlor
      cantF:=cantF + 1
    mientras(HayPapelEnLaEsquina)
      tomarPapel
      cantP:=cantP + 1
  fin
  
  proceso Verificar(E cantF:numero; E cantP:numero; ES cantEnviar:numero)
  comenzar
    si(cantF > cantP)
      cantEnviar:=cantEnviar + 1
  fin

  proceso HacerEscalon(E alto:numero; ES cantEnviar:numero)
  variables
    cantF,cantP:numero
  comenzar
    Inicializar(cantF,cantP)
    repetir alto
      JuntarEsquina(cantF,cantP)
      mover
    derecha
    repetir 1
      JuntarEsquina(cantF,cantP)
      mover
    Verificar(cantF,cantP,cantEnviar)
    Izquierda
  fin

areas
  Escalera: AreaC(1,1,40,40)
robots
  robot trabajador
  variables
    alto:numero
    cantEnviar:numero
  comenzar
    cantEnviar:=0
    Random(alto,1,5)
    repetir 4
      HacerEscalon(alto,cantEnviar)
    EnviarMensaje(cantEnviar,rj)
  fin

  robot robotJefe
  variables
    aux,total:numero
  comenzar
    aux:=0
    total:=0
    repetir 3
      RecibirMensaje(aux,*)
      total:=total + aux
    Informar('CANTIDAD-TOTAL',total)
  fin
variables  
  r1:trabajador
  r2:trabajador
  r3:trabajador
  rj:robotJefe
comenzar
  AsignarArea(r1,Escalera)
  AsignarArea(r2,Escalera)
  AsignarArea(r3,Escalera)
  AsignarArea(rj,Escalera)
  Iniciar(r1,2,1)
  Iniciar(r2,7,1)
  Iniciar(r3,12,1)
  Iniciar(rj,1,1)
fin
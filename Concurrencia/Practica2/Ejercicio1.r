programa exercise1
procesos
  proceso JuntarFlores(ES cantFlores:numero)
  comenzar
    mientras(HayFlorEnLaEsquina)
      cantFlores:=cantFlores + 1
      tomarFlor
  fin
areas
  areaRS: AreaP(2,1,2,1)
  area1: AreaPC(1,1,1,10)
  area2: AreaP(2,11,2,20)
  area3: AreaP(3,21,3,30)
  area4: AreaP(4,31,4,40)
  area5: AreaP(5,41,5,50)
  area6: AreaP(6,51,6,60)
robots
  robot competidores
  variables
    id,cantFlores:numero
  comenzar
    RecibirMensaje(id,rs)
    cantFlores:=0
    repetir 9
      JuntarFlores(cantFlores)
      mover
      JuntarFlores(cantFlores)
    EnviarMensaje(id,rs)
    EnviarMensaje(cantFlores,rs)
  fin
  
  robot supervisor
  variables
    idRobot,idMax,max,aux:numero
  comenzar
    EnviarMensaje(1,r1)
    EnviarMensaje(2,r2)
    EnviarMensaje(3,r3)
    EnviarMensaje(4,r4)
    EnviarMensaje(5,r5)
    EnviarMensaje(6,r6)

    max:=0
    repetir 6
      RecibirMensaje(idRobot, *)
      si(idRobot = 1)
        RecibirMensaje(aux,r1)
      sino 
        si(idRobot = 2)
          RecibirMensaje(aux,r2)
        sino 
          si(idRobot = 3)
            RecibirMensaje(aux,r3)
          sino 
            si(idRobot = 4)
              RecibirMensaje(aux,r4)
            sino 
              si(idRobot = 5)
                RecibirMensaje(aux,r5)
              sino
                RecibirMensaje(aux,r6)
      si(aux > max)
        max:=aux
        idMax:=idRobot
    Informar('Ganador',idRobot)
    Informar('Cantidad',max)
  fin

variables
  r1: competidores
  r2: competidores
  r3: competidores
  r4: competidores
  r5: competidores
  r6: competidores
  rs: supervisor
comenzar
  AsignarArea(r1,area1)
  AsignarArea(r2,area2)
  AsignarArea(r3,area3)
  AsignarArea(r4,area4)
  AsignarArea(r5,area5)
  AsignarArea(r6,area6)
  AsignarArea(rs,areaRS)
  Iniciar(r1,1,1)
  Iniciar(r2,2,11)
  Iniciar(r3,3,21)
  Iniciar(r4,4,31)
  Iniciar(r5,5,41)
  Iniciar(r6,6,51)
  Iniciar(rs,2,1)
fin





programa exerciseFour
areas
  cuadrante: AreaP(5,5,20,20)
  areaF: AreaP(1,1,1,1)
  area1: AreaP(10,1,10,1)
  area2: AreaP(12,1,12,1)
  area3: AreaP(14,1,14,1)
  area4: AreaP(16,1,16,1)

robots
  robot Recolectores
  variables
    idRobot:numero
    acceso:boolean
    avInicial,caInicial,av,ca:numero
  comenzar
    avActual:=PosAv
    caActual:=PosCa
    RecibirMensaje(idRobot,rf)
    derecha
    Random(av,5,20)
    Random(ca,5,20)
    RecibirMensaje(acceso,rf)
    si(acceso)
      Pos(av,ca)
      mover
    Pos(avActual,caActual)
  fin

  robot Fiscalizador
  variables
    id:numero
  comenzar
    derecha
    EnviarMensaje(1,r1)
    EnviarMensaje(2,r2)
    EnviarMensaje(3,r3)
    EnviarMensaje(4,r4)
    Random(id,1,4)
    Informar('ID-ELEGIDO',id)
    si(id = 1)
      EnviarMensaje(v,r1)
      EnviarMensaje(F,r2)
      EnviarMensaje(F,r3)
      EnviarMensaje(F,r4)
    sino 
      si(id = 2)
        EnviarMensaje(F,r1)
        EnviarMensaje(V,r2)
        EnviarMensaje(F,r3)
        EnviarMensaje(F,r4)
      sino 
        si(id = 3)
          EnviarMensaje(F,r1)
          EnviarMensaje(F,r2)
          EnviarMensaje(V,r3)
          EnviarMensaje(F,r4)
        sino
          EnviarMensaje(F,r1)
          EnviarMensaje(F,r2)
          EnviarMensaje(F,r3)
          EnviarMensaje(V,r4)
  fin

variables
  r1:Recolectores
  r2:Recolectores
  r3:Recolectores
  r4:Recolectores
  rf:Fiscalizador
comenzar
  AsignarArea(rf,areaF)
  AsignarArea(r1,area1)
  AsignarArea(r2,area2)
  AsignarArea(r3,area3)
  AsignarArea(r4,area4)

  AsignarArea(r1,cuadrante)
  AsignarArea(r2,cuadrante)
  AsignarArea(r3,cuadrante)
  AsignarArea(r4,cuadrante)
  
	Iniciar(rf,1,1)
  Iniciar(r1,10,1)
  Iniciar(r2,12,1)
  Iniciar(r3,14,1)
  Iniciar(r4,16,1)
fin
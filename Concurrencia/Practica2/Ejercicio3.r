programa equipos
procesos
  proceso JuntarPapeles(ES cantP:numero)
  comenzar
    mientras(HayPapelEnLaEsquina)
      tomarPapel
      cantP:=cantP + 1
  fin

  proceso JuntarFlores(ES cantF:numero)
  comenzar
    mientras(HayFlorEnLaEsquina)
      tomarFlor
      cantF:=cantF + 1
  fin

areas
  equipoA: AreaPC(1,1,20,1)
  equipoB: AreaPC(1,5,20,5)

robots
  robot tipoA1
  variables
    cantP:numero 
  comenzar
    cantP:=0
    derecha
    repetir 9
      JuntarPapeles(cantP)
      mover
    JuntarPapeles(cantP)
    EnviarMensaje(cantP,a2)
  fin
  robot tipoA2
  variables
    cantP,cantPA1:numero
  comenzar
    cantP:=0
    RecibirMensaje(cantPA1, a1)
    derecha
    repetir 9
      JuntarPapeles(cantP)
      mover
    JuntarPapeles(cantP)
    cantP:=cantP + cantPA1
    Informar('EquipoA-CantidadFlores',cantP)
  fin

  robot tipoB1
  variables
    cantF:numero
  comenzar
    cantF:=0
    derecha
    repetir 9
      JuntarFlores(cantF)
      mover
    JuntarFlores(cantF)
    EnviarMensaje(cantF, b2)
  fin
  robot tipoB2
  variables
    cantF,cantFB1:numero
  comenzar
    cantF:=0
    RecibirMensaje(cantFB1,b1)
    derecha
    repetir 9
      JuntarFlores(cantF)
      mover
    JuntarFlores(cantF)
    cantF:=cantF + cantFB1
    Informar('EquipoB-CantidadPapeles',cantF)
  fin
variables
  a1:tipoA1
  a2:tipoA2
  b1:tipoB1
  b2:tipoB2
comenzar
  AsignarArea(a1,equipoA)
  AsignarArea(a2,equipoA)
  AsignarArea(b1,equipoB)
  AsignarArea(b2,equipoB)
  Iniciar(a1,1,1)
  Iniciar(a2,11,1)
  Iniciar(b1,1,5)
  Iniciar(b2,11,5)
fin
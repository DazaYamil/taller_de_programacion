package POO.Practica2;
import java.util.Scanner;

public class Ejercicio4 {
   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);

      int dimF = 5;
      int dimC = 4;
      int df = 0;
      int dc = 0;
      Persona [][] casting = new Persona[dimF][dimC];

      //Inicializar la matriz
      for(int f = 0; f < dimF; f++){
         for(int c = 0; c < dimC; c++){
            casting[f][c] = null;
         }
      }

      String nombre = "";
      int dni = 0;
      int edad = 0;

      System.out.print("- Nombre: "); 
      nombre = sc.nextLine();

      while(((df < dimF) && (dc < dimC)) && (!nombre.equals("zzz"))){
         System.out.print("- DNI: "); dni = sc.nextInt();
         sc.nextLine();
         System.out.print("- Edad: "); edad = sc.nextInt();
         sc.nextLine();
         
         casting[df][dc] = new Persona(nombre, dni, edad);
         dc++;

         if(dc == dimC){
            dc = 0;
            df++;
         }

         System.out.print("- Nombre: "); nombre = sc.nextLine();
      }

      System.out.println(" --- Mostando Datos del Casting --- ");
      for (int f = 0; f < df; f++) {
         for (int c = 0; c < dc; c++) {
            System.out.println(casting[f][c].presentacion() + " | ");
         }
         System.out.println();
      }
      System.out.println();


      sc.close();
   }
}

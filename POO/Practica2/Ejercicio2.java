package POO.Practica2;
import java.util.Scanner;

public class Ejercicio2 {
   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);

      int dimF = 10;
      int dimL = 0;
      Persona [] vector = new Persona[dimF];

      //Inicializar el vector en valores Null ya que lo que almacena es un objeto
      for(int i=0; i < dimF; i++){
         vector[i] = null;
      }

      System.out.print("- Introduzca el nombre: ");
      String nombre = sc.nextLine();
      System.out.print("- Introduzca el DNI: ");
      int dni = sc.nextInt();
      System.out.print("- Introduzca la edad: ");
      int edad = sc.nextInt();

      while ((dimL < dimF) && (edad != 0)) {
         vector[dimL] = new Persona(nombre, dni, edad);
         dimL++;

         System.out.print("- Introduzca el nombre: ");
         nombre = sc.nextLine();

         System.out.print("- Introduzca el DNI: ");
         dni = sc.nextInt();

         System.out.print("- Introduzca la edad: ");
         edad = sc.nextInt();
      }

      for(int i=0; i < dimL; i++){
         System.out.println(" * Datos Persona numero " + (i+1));
         System.out.println(vector[i].presentacion());
         System.out.println();
      }


      sc.close();
   }
}

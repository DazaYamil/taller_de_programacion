package POO.Practica2;

public class Persona {
   private String nombre;
   private int dni;
   private int edad;

   public Persona(String nombre, int dni, int edad){
      this.nombre = nombre;
      this.dni = dni;
      this.edad = edad;
   }

   //SET AND GET NOMBRE
   public void setNombre(String nombre) {
      this.nombre = nombre;
   }
   public String getNombre() {
      return nombre;
   }

   //SET AND GET DNI
   public void setDni(int dni) {
      this.dni = dni;
   }
   public int getDni() {
      return dni;
   }

   //SET AND GET EDAD
   public void setEdad(int edad) {
      this.edad = edad;
   }
   public int getEdad() {
      return edad;
   }

   public String presentacion(){
      return "Hi, My name is " + this.nombre + ", I am " + this.edad + " years old and my dni is " + this.dni;
   }

}

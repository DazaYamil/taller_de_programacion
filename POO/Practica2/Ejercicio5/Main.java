package POO.Practica2.Ejercicio5;
import java.util.Scanner;

public class Main {
   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      int dimF = 5;
      int dimL = 0;
      Partido [] campeonato = new Partido[dimF];

      //Inicializar vector
      for (int i = 0; i < dimF; i++) {
         campeonato[i] = null;
      }


      String nombreLocal = "";
      String nombreVisitante = "";
      int golesLocal = 0;
      int golesVisitante = 0;
      System.out.print("- Nombre de Equipo Local: "); 
      nombreLocal = sc.nextLine();
      System.out.print("- Nombre de Equipo Visitante: "); 
      nombreVisitante = sc.nextLine();

      //Cargar Campeonato
      while((dimL < dimF) && (!nombreVisitante.equals("zzz"))){
         System.out.print("- Goles Local: "); 
         golesLocal = sc.nextInt();
         sc.nextLine();
         System.out.print("- Goles Visitante: "); 
         golesVisitante = sc.nextInt();
         sc.nextLine();

         campeonato[dimL] = new Partido(nombreLocal, nombreVisitante, golesLocal, golesVisitante);
         dimL++;
         System.out.println();

         System.out.print("- Nombre de Equipo Local: "); 
         nombreLocal = sc.nextLine();
         System.out.print("- Nombre de Equipo Visitante: "); 
         nombreVisitante = sc.nextLine();
      }

      int partidosGanadorRiver = 0;
      int cantidadGolesBocaLocal = 0;

      for (int i = 0; i < dimL; i++) {
         Partido p = campeonato[i];
         System.out.println(p.toString());
         if(p.getGanador().equals("RIVER")){
               partidosGanadorRiver++;
         }
         if(p.getLocal().equals("BOCA")){
            cantidadGolesBocaLocal += p.getGolesLocal();
         }
      }

      System.out.println();
      System.out.println("CANTIDAD DE PARTIDOS GANADOS POR EL EQUIPO RIVER PLATE : " + partidosGanadorRiver);
      System.out.println("CANTIDAD DE GOLES POR EL EQUIPO BOCA JUNIORS DE LOCAL : " + cantidadGolesBocaLocal);

      sc.close();
   }
}

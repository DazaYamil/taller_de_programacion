package POO.Practica2.Ejercicio5;

public class Partido {
   private String local;
   private String visitante;
   private int golesLocal;
   private int golesVisitante;

   public Partido(){}

   public Partido(String local, String visitante, int golesLocal, int golesVisitante){
      this.local = local;
      this.visitante = visitante;
      this.golesLocal = golesLocal;
      this.golesVisitante = golesVisitante;
   }

   public String getLocal(){
      return this.local;
   }
   public String getVisitante(){
      return this.visitante;
   }
   public int getGolesLocal(){
      return this.golesLocal;
   }
   public int getGolesVisitante(){
      return this.golesVisitante;
   }
   public void setLocal(String xLocal){
      this.local = xLocal;
   }
   public void setVisitante(String xVisitante){
      this.visitante = xVisitante;
   }
   public void setGolesVisitante(int goles){
      this.golesVisitante = goles;
   }
   public void setGolesLocal(int goles){
      this.golesLocal = goles;
   }

   //Hay ganador
   public boolean hayGanador(){
      return ((this.getGolesLocal() > this.getGolesVisitante()) || (this.getGolesVisitante() > this.getGolesLocal()));
   }

   //Hay empate
   public boolean hayEmpate(){
      return this.golesLocal == this.getGolesVisitante();
   }

   //Get Ganador
   public String getGanador(){
      String aux = "";
      if(this.hayEmpate()){
         aux = "NO HUBO GANADOR - AMBOS EQUIPOS EMPATARON";
      }
      if(this.getGolesLocal() > this.getGolesVisitante()){
         aux = this.getLocal();
      }else{
         aux = this.getVisitante();
      }
      return aux;
   }

   //To String
   public String toString() {
      String aux = "EQUIPO LOCAL: " + this.local + " - " + this.golesLocal + " Goles VS. EQUIPO VISITANTE: " + this.visitante + " -  " + this.golesVisitante + " Goles";
      return aux;
   }

}

package POO.Practica4.Ejercicio3;

public class Trabajador extends Persona {
   private String tarea;

   public Trabajador(String nombre, int dni, int edad, String tarea){
      super(nombre, dni, edad);
      this.tarea = tarea;
   }
   
   public String getTarea() {
      return tarea;
   }
   public void setTarea(String tarea) {
      this.tarea = tarea;
   }

   @Override
   public String toString() {
      return super.toString() + " " + this.getTarea();
   }

}

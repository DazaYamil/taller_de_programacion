package POO.Practica4.Ejercicio3;

public class Main {
   public static void main(String[] args) {
      Persona p1 = new Persona("Belen", 1234, 25);
      Trabajador t1 = new Trabajador("Yamil", 1241, 25, "Software Developer Backend");
      System.out.println(" -------------------------------------- ");
      System.out.println(p1.toString());
      System.out.println(t1.toString());
      System.out.println(" -------------------------------------- ");
   }
}

package POO.Practica4.Ejercicio3;

public class Persona {
   private String nombre;
   private int dni, edad;

   public Persona(String nombre, int dni, int edad){
      this.nombre = nombre;
      this.dni = dni;
      this.edad = edad;
   }

   public String getNombre() {
      return nombre;
   }
   public void setNombre(String nombre) {
      this.nombre = nombre;
   }
   public int getDni() {
      return dni;
   }
   public void setDni(int dni) {
      this.dni = dni;
   }
   public int getEdad() {
      return edad;
   }
   public void setEdad(int edad) {
      this.edad = edad;
   }

   public String toString() {
      return "My name is " + this.getNombre() + " My dni is " + this.getDni() + " and I have " + this.getEdad();
   }

}

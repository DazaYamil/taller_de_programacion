package POO.Practica4.Ejercicio1;

public class Triangulo extends Figura {
   private int lado1, lado2, lado3;

   public Triangulo(String colorRelleno, String colorLinea, int lado1, int lado2, int lado3){
      super(colorRelleno, colorLinea);
      setLado1(lado1);
      setLado2(lado2);
      setLado3(lado3);
   }

   public void setLado1(int lado1) {
      this.lado1 = lado1;
   }
   public void setLado2(int lado2) {
      this.lado2 = lado2;
   }
   public void setLado3(int lado3) {
      this.lado3 = lado3;
   }
   public int getLado1() {
      return lado1;
   }
   public int getLado2() {
      return lado2;
   }
   public int getLado3() {
      return lado3;
   }

   @Override
   public String toString() {
      String aux = super.toString() + "Lado1: " + getLado1() + " Lado2: " + getLado2() + " Lado3: " + getLado3();
      return aux;
   }

   public double calcularArea(){
      // Calcular el semiperímetro
      double s = (getLado1() + getLado2() + getLado3()) / 2;
      // Aplicar la fórmula de Herón
      double area = Math.sqrt(s * (s - getLado1()) * (s - getLado2()) * (s - getLado3()));
      return area;
   }
   public double calcularPerimetro(){
      return (getLado1() + getLado2() + getLado3());
   }
}

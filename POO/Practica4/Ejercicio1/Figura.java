package POO.Practica4.Ejercicio1;

public abstract class Figura {
   private String colorRelleno;
   private String colorLinea;

   public Figura(String colorRelleno, String colorLinea){
      setColorRelleno(colorRelleno);
      setColorLinea(colorLinea);
   }

   public String toString() {
      String aux = "CR: " + this.getColorRelleno() + " - CL: " + this.getColorLinea() + " - AREA: " + this.calcularArea() + " - PERIMETRO: " + this.calcularPerimetro();
      return aux;
   }

   public void despintar(){
      setColorRelleno("blanco");
      setColorLinea("negro");
   }

   public String getColorRelleno() {
      return colorRelleno;
   }
   public void setColorRelleno(String colorRelleno) {
      this.colorRelleno = colorRelleno;
   }
   public String getColorLinea() {
      return colorLinea;
   }
   public void setColorLinea(String colorLinea) {
      this.colorLinea = colorLinea;
   }

   //Metodos abstractos
   public abstract double calcularArea();
   public abstract double calcularPerimetro();

}

package POO.Practica4.Ejercicio2;

public class Main {
   public static void main(String[] args) {
      Empleado j1 = new Jugador("Alan", 500, 2, 5, 10);
      Empleado e1 = new Entrenador("Jose", 200000, 3, 3);

      System.out.println(j1.toString());
      System.out.println(e1.toString());
   }
}

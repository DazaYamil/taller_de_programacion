package POO.Practica4.Ejercicio2;

public abstract class Empleado {
   private String nombre;
   private double sueldoBasico;
   private int antiguedad;

   public Empleado(String nombre, double sueldoBasico, int antiguedad){
      this.nombre = nombre;
      this.sueldoBasico = sueldoBasico;
      this.antiguedad = antiguedad;
   }
   public void setNombre(String nombre) {
      this.nombre = nombre;
   }
   public void setSueldoBasico(double sueldoBasico) {
      this.sueldoBasico = sueldoBasico;
   }
   public void setAntiguedad(int antiguedad) {
      this.antiguedad = antiguedad;
   }
   public String getNombre() {
      return nombre;
   }
   public double getSueldoBasico() {
      return sueldoBasico;
   }
   public int getAntiguedad() {
      return antiguedad;
   }

   public double extraSueldo(){
      return this.getSueldoBasico() + ((this.getSueldoBasico() * 0.10 * this.getAntiguedad()));
   }

   public abstract double calcularEfectividad();
   public abstract double calcularSueldoACobrar();

   @Override
   public String toString() {
      return "My name is " + this.getNombre() + " - My Sueldo is " + this.calcularSueldoACobrar() + " - My Efectividad is " + this.calcularEfectividad();
   }

}

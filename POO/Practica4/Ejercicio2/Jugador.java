package POO.Practica4.Ejercicio2;

public class Jugador extends Empleado{
   private int partidosJugados;
   private int golesAnotados;

   public Jugador(String nombre, double sueldoBasico, int antiguedad, int partidosJugados, int golesAnotados){
      super(nombre, sueldoBasico, antiguedad);
      this.partidosJugados = partidosJugados;
      this.golesAnotados = golesAnotados;
   }

   public void setGolesAnotados(int golesAnotados) {
      this.golesAnotados = golesAnotados;
   }
   public void setPartidosJugados(int partidosJugados) {
      this.partidosJugados = partidosJugados;
   }
   public int getGolesAnotados() {
      return golesAnotados;
   }
   public int getPartidosJugados() {
      return partidosJugados;
   }

   @Override
   public double calcularEfectividad() {
      return this.getGolesAnotados() / this.getPartidosJugados();
   }
   private double sueldoAdicional(){
      double promedio = this.getGolesAnotados() / this.getPartidosJugados();
      if(promedio > 0.5) return this.getSueldoBasico();
      return 0;
   }

   @Override
   public double calcularSueldoACobrar() {
      return this.extraSueldo() + sueldoAdicional();
   }
}


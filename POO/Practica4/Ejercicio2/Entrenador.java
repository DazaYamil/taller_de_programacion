package POO.Practica4.Ejercicio2;

public class Entrenador extends Empleado {
   private int campeonatosGanados;

   public Entrenador(String nombre, double sueldoBasico, int antiguedad, int campeonatosGanados){
      super(nombre, sueldoBasico, antiguedad);
      this.campeonatosGanados = campeonatosGanados;
   }

   public void setCampeonatosGanados(int campeonatosGanados) {
      this.campeonatosGanados = campeonatosGanados;
   }
   public int getCampeonatosGanados() {
      return campeonatosGanados;
   }

   @Override
   public double calcularEfectividad() {
      return this.getCampeonatosGanados() / this.getAntiguedad();
   }

   private double cobroPorCampeonatos(int antiguedad){
      double aux = 0;
      if(antiguedad >= 1 || antiguedad <= 4){
         aux = 5000;
      }else if(antiguedad >= 5 || antiguedad <= 10){
         aux = 30000;
      }else if(antiguedad > 10){
         aux = 50000;
      }
      return aux;
   }
   @Override
   public double calcularSueldoACobrar() {
      return this.extraSueldo() + cobroPorCampeonatos(this.getCampeonatosGanados());
   }
}

package POO.Practica3.Ejercicio2;

public class Libro {
   private String titulo;
   private Autor primerAutor;
   private String editorial;
   private int anioEdicion;
   private int ISBN;
   private double precio;

   public Libro(){};
   public Libro(String titulo, Autor primerAutor, String editorial, int anio, int isbn, double precio){
      this.titulo = titulo;
      this.primerAutor = primerAutor;
      this.editorial = editorial;
      this.anioEdicion = anio;
      this.ISBN = isbn;
      this.precio = precio;
   }

   //GET AND SET TITULO
   public String getTitulo() {
      return titulo;
   }
   public void setTitulo(String titulo) {
      this.titulo = titulo;
   }

   //GET AND SET PRIMERAUTOR
   public Autor getPrimerAutor() {
      return primerAutor;
   }
   public void setPrimerAutor(Autor primerAutor) {
      this.primerAutor = primerAutor;
   }

   //GET AND SET EDITORIAL
   public String getEditorial() {
      return editorial;
   }
   public void setEditorial(String editorial) {
      this.editorial = editorial;
   }

   //GET AND SET ANIOEDICION
   public int getAnioEdicion() {
      return anioEdicion;
   }
   public void setAnioEdicion(int anioEdicion) {
      this.anioEdicion = anioEdicion;
   }

   //GET AND SET ISBN
   public int getISBN() {
      return ISBN;
   }
   public void setISBN(int iSBN) {
      ISBN = iSBN;
   }

   //GET AND SET PRECIO
   public double getPrecio() {
      return precio;
   }
   public void setPrecio(double precio) {
      this.precio = precio;
   }

   //TO STRING
   public String toString(){
      return "* TITULO DEL LIBRO: " + this.titulo + "\n* AUTOR: " + this.primerAutor.toString() + "\n* EDITORIAL: " + this.editorial + "\n* AÑO EDICION: " + this.anioEdicion + "\n* ISBN: " + this.ISBN + "\n* PRECIO: $" + this.precio;
   }
}

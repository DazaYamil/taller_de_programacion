package POO.Practica3.Ejercicio2;

public class Autor {
   private String nombre;
   private String biografia;
   private String origen;

   public Autor(){};
   public Autor(String nombre, String biografia, String origen){
      this.nombre = nombre;
      this.biografia = biografia;
      this.origen = origen;
   }

   //GET AND SET NOMBRE
   public String getNombre() {
      return nombre;
   }
   public void setNombre(String nombre) {
      this.nombre = nombre;
   }

   //SET AND GET BIOGRAFIA
   public String getBiografia() {
      return biografia;
   }
   public void setBiografia(String biografia) {
      this.biografia = biografia;
   }

   //SET AND GET ORIGEN
   public String getOrigen() {
      return origen;
   }
   public void setOrigen(String origen) {
      this.origen = origen;
   }

   public String toString(){
      return "Nombre: " + this.nombre + " Biografia " + this.biografia + " Origen: " + this.origen;
   }
}

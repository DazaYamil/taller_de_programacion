package POO.Practica3.Ejercicio3;

import POO.Practica3.Ejercicio2.Libro;

public class Estante {
   private int dimF;
   private int dimL;
   private Libro [] estante;

   public Estante(){
      dimF = 10;
      dimL = 0;
      estante = new Libro[dimF];
   }

   public int cantidadLibrosAlmacenados(){
      return this.dimL;
   }

   public boolean estanteLleno(){
      return dimL == dimF;
   }

   public void agregarLibro(Libro l1){
      if(dimL < dimF){
         estante[dimL] = l1;
         dimL++;
      }
   }

   private int buscarIndice(String titulo){
      int indiceLibro = -1;
      for (int i = 0; i < dimL; i++) {
         if(estante[dimL].getTitulo().equals(titulo)){
            indiceLibro = dimL;
         }
      }
      return indiceLibro;
   }

   public Libro buscarLibro(String titulo){
      int i = buscarIndice(titulo);
      if(i != -1){
         return estante[i];
      }
      return null;
   }

}

package POO.Practica3.Ejercicio3;
import POO.Practica3.Ejercicio2.Libro;
import POO.Practica3.Ejercicio2.Autor;


public class Main {
   public static void main(String[] args) {
      Estante e = new Estante();
      Autor a1 = new Autor("Robert C. Martin", "Programmer", "P.");
      Libro l1 = new Libro("Clean Code", a1, "SSA", 2011, 203, 12000);
      e.agregarLibro(l1);

      Autor a2 = new Autor("Jose Maria Gertrudix", "Programmer", "JA.");
      Libro l2 = new Libro("JAVA 17", a2, "TTA", 2017, 203, 15500);
      e.agregarLibro(l2);
      

   }
}

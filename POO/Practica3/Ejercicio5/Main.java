package POO.Practica3.Ejercicio5;

import java.text.DecimalFormat;

public class Main {
   public static void main(String[] args) {
      DecimalFormat f = new DecimalFormat("#.00");
      Circulo c1 = new Circulo(12, "Blue", "White");
      Circulo c2 = new Circulo(16, "Yellow", "Black");

      System.out.println(c1.informacionCirculo());
      System.out.println(c2.informacionCirculo());
      System.out.println();

      System.out.println("PERIMETRO C1: " + f.format(c1.calcularPerimetro()) + " | AREA: " + f.format(c1.calcularArea()));
      System.out.println("PERIMETRO C2: " + f.format(c2.calcularPerimetro()) + " | AREA: " + f.format(c2.calcularArea()));
   }
}

package POO.Practica3.Ejercicio5;

public class Circulo {
   private double radio;
   private String colorRelleno;
   private String colorLinea;

   public Circulo(double radio, String colorRelleano, String colorLinea){
      this.radio = radio;
      this.colorRelleno = colorRelleano;
      this.colorLinea = colorLinea;
   }

   //GET AND SET RADIO
   public double getRadio() {
      return radio;
   }
   public void setRadio(double radio) {
      this.radio = radio;
   }

   //GET AND SET COLOR RELLENO   
   public String getColorRelleno() {
      return colorRelleno;
   }
   public void setColorRelleno(String colorRelleno) {
      this.colorRelleno = colorRelleno;
   }

   //GET AND SET COLOR LINEA
   public String getColorLinea() {
      return colorLinea;
   }
   public void setColorLinea(String colorLinea) {
      this.colorLinea = colorLinea;
   }

   public double calcularPerimetro(){
      return Math.PI * (this.radio * 2);
   }
   public double calcularArea(){
      return (Math.PI * this.radio) * 2;
   }

   public String informacionCirculo(){
      return "Radio: " + this.getRadio() + " | Color de Rellano: " + this.getColorRelleno() + " | Color de Linea: " + this.getColorLinea();
   }
}

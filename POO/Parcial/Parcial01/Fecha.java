package POO.Parcial.Parcial01;

public class Fecha {
   private Jugador[] jugadores;
   private int dimF;
   private int dimL = 0;

   public Fecha(int dimF){
      this.dimF = dimF;
      this.jugadores = new Jugador[this.dimF];
   }

   public int getDimL() {
      return dimL;
   }
   public Jugador[] getJugadores() {
      return jugadores;
   }
   public int getDimF() {
      return dimF;
   }
}

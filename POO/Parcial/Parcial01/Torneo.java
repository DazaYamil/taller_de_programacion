package POO.Parcial.Parcial01;

public class Torneo {
   private String nombre;
   private Fecha[] fechas;
   private int[] cantidadGoleadores;
   private int dimF;

   public Torneo(String nombre, int dimF){
      this.nombre = nombre;
      this.dimF = dimF;
      this.fechas = new Fecha[dimF];
   }

   public String getNombre() {
      return nombre;
   }
   public int getDimF() {
      return dimF;
   }
   public Fecha[] getFechas() {
      return fechas;
   }
}

package POO.Parcial.Parcial01;

public class Jugador {
   private String nombre;
   private String nombreEquipo;
   private int cantidadGoles;

   public Jugador(String nombre, String nombreEquipo, int cantidadGoles){
      this.nombre = nombre;
      this.nombreEquipo = nombreEquipo;
      this.cantidadGoles = cantidadGoles;
   }

   public String getNombre() {
      return nombre;
   }
   public String getNombreEquipo() {
      return nombreEquipo;
   }
   public int getCantidadGoles() {
      return cantidadGoles;
   }
}

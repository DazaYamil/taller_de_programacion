package POO.RepasoFinal.Ejercicio1;

public class Proyecto {
   private String nombre;
   private int codigo;
   private String director;
   private Investigador [] investigadores;
   private int dimF = 10;
   private int dimL = 0;

   public Proyecto(String nombre, int codigo, String director){
      this.nombre = nombre;
      this.codigo = codigo;
      this.director = director;
      this.investigadores = new Investigador[this.dimF];
   }

   public String getNombre() {
      return nombre;
   }
   public int getCodigo() {
      return codigo;
   }
   public String getDirector() {
      return director;
   }

   public void addInvestigador(Investigador unInvestigador){
      if(this.dimL < this.dimF){
         investigadores[this.dimL] = unInvestigador;
         dimL++;
      }
   }

   public double montoTotalProyecto(){
      double aux = 0;
      for (int i = 0; i < this.dimL; i++) {
         aux += investigadores[i].montoTotalInvestigador();
      }
      return aux;
   }

   public void otorgarTodos(String nombre){
      int i = 0;
      boolean ok = false;
      while((i < this.dimL) && (!(ok))){
         if (investigadores[i].getNombre().equals(nombre)){
            investigadores[i].otorgarI();
            ok = true;
         }else{ 
            i++;
         }
      }
      if (!(ok)) System.out.print("No se encontró");
   }

   private String toStringInvestigadores(){
      String aux = "";
      for (int i = 0; i < this.dimL; i++) {
         aux += " NRO " + (i+1) + ": \n";
         aux += investigadores[i].toString();
      }
      return aux;
   }

   public String toString() {
      String aux = "NOMBRE PROYECTO: " + this.getNombre() + " - CODIGO: " + this.getCodigo() + " - DIRECTOR: " + this.getCodigo() + " - DINERO OTORGADO: " + this.montoTotalProyecto();
      if(this.dimL == 0){
         aux += " - SIN INVESTIGADORES.";
      }else{
         aux += " - INVESTIGADORES: " + this.toStringInvestigadores();
      }
      return aux;
   }

}

package POO.RepasoFinal.Ejercicio1;

public class Subsidio {
   private double montoPedido;
   private String motivo;
   private boolean isOtorgado;

   public Subsidio(double montoPedido, String motivo){
      this.montoPedido = montoPedido;
      this.motivo = motivo;
      this.isOtorgado = false;
   }
   public String getMotivo() {
      return motivo;
   }

   public double getMontoPedido(){
      if(isOtorgado){
         return this.montoPedido;
      }
      return 0;
   }

   public void setOtorgado(boolean isOtorgado) {
      this.isOtorgado = isOtorgado;
   }
}

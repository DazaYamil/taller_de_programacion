package POO.RepasoFinal.Ejercicio1;

public class Investigador {
   private String nombre;
   private int categoria;
   private String especialidad;
   private Subsidio [] subsidios;
   private int dimF = 5;
   private int dimL = 0;

   public Investigador(String nombre, int categoria, String especialidad){
      this.nombre = nombre;
      this.categoria = categoria;
      this.especialidad = especialidad;
      this.subsidios = new Subsidio[this.dimF];
   }

   public int getCategoria() {
      return categoria;
   }
   public String getEspecialidad() {
      return especialidad;
   }

   public String getNombre() {
      return nombre;
   }

   public void addSubsidio(Subsidio unSubsidio){
      if(this.dimL < this.dimF){
         subsidios[this.dimL] = unSubsidio;
         this.dimL++;
      }
   }

   public double montoTotalInvestigador(){
      double aux = 0;
      for (int i = 0; i < this.dimL; i++) {
         aux += subsidios[i].getMontoPedido();
      }
      return aux;
   }

   public void otorgarI(){
      for (int i = 0; i < this.dimL; i++) {
         subsidios[i].setOtorgado(true);
      }
   }

   public String toString() {
      String aux = " nombre: " + this.getNombre() + " - categoria: " + this.getCategoria() + " - especialidad: " + this.getEspecialidad() + " TOTAL OTORGADO: " + this.montoTotalInvestigador();
      return aux;
   }
}

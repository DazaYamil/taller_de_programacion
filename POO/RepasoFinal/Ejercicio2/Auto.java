package POO.RepasoFinal.Ejercicio2;

public class Auto {
   private String nombreDueno;
   private String patente;

   public Auto(String nombre, String patente){
      this.nombreDueno = nombre;
      this.patente = patente;
   }

   public String getNombreDueno() {
      return nombreDueno;
   }
   public String getPatente() {
      return patente;
   }

   public String toString() {
      
      return " Dueño: " + this.nombreDueno + " - Patente: " + this.patente;
   }

}

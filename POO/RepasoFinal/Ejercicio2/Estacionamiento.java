package POO.RepasoFinal.Ejercicio2;

public class Estacionamiento {
   private String nombre;
   private String direccion;
   private String horaApertura;
   private String horaCierre;
   private Auto [][] autos;
   private int dimF;
   private int dimC;

   //Constructor1
   public Estacionamiento(String nombre, String direccion){
      this.nombre = nombre;
      this.direccion = direccion;
      this.horaApertura = "08:00";
      this.horaCierre = "21:00";
      this.dimF = 3;
      this.dimC = 3;
      this.autos = new Auto[dimF][dimC];
      inicializarLugaresAutos();
   }
   //Constructor2
   public Estacionamiento(String nombre, String direccion,String horaApertura, String horaCierre, int dimFila, int dimColumna ){
      this.nombre = nombre;
      this.direccion = direccion;
      this.horaApertura = horaApertura;
      this.horaCierre = horaCierre;
      this.dimF = dimFila;
      this.dimC = dimColumna;
      this.autos = new Auto[dimF][dimC];
      inicializarLugaresAutos();
   }

   private void inicializarLugaresAutos(){
      for (int f = 0; f < this.dimF; f++) {
         for (int c = 0; c < this.dimC; c++) {
            autos[f][c] = null;
         }
      }
   }

   //Agregar un Auto al estacionamiento
   public void agregarAuto(Auto unAuto, int x, int y){
      this.autos[x][y] = unAuto;
   }

   //Buscar auto por Patente
   public String buscarPatente(String patente) {
      for (int f = 0; f < this.dimF; f++) {
         for (int c = 0; c < this.dimC; c++) {
            Auto auto = this.autos[f][c];
            if ((auto != null) && (auto.getPatente().equals(patente))){
               // La patente se encontró, devolver el número de piso y plaza
               return "Auto encontrado en el piso " + (f + 1) + ", plaza " + (c + 1);
            }
         }
      }
      // La patente no se encontró
      return "Auto Inexistente";
   }

   //Representacion en String del Estacionamiento
   public String toString(){
      String aux = "";
      for (int f = 0; f < this.dimF; f++) {
         for (int c = 0; c < this.dimC; c++) {
            if (this.autos[f][c] == null){
               aux += "\n Piso " + (f+1) + " - Plaza " + (c+1) + " LIBRE ";
            }else{
               aux += "\n Piso " + (f+1) + " - Plaza " + (c+1) + " OCUPADO -> " + this.autos[f][c].toString();
            }
         }
         aux += "\n --------------------------";
      }
      return aux;
   }

   public String getNombre() {
      return nombre;
   }
   public String getDireccion() {
      return direccion;
   }
   public String getHoraApertura() {
      return horaApertura;
   }
   public String getHoraCierre() {
      return horaCierre;
   }
   public Auto[][] getAutos() {
      return autos;
   }
}

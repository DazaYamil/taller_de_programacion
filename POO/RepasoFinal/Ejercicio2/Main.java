package POO.RepasoFinal.Ejercicio2;

public class Main {
   public static void main(String[] args) {
      Estacionamiento e1 = new Estacionamiento("Plaza Once", "Plaza Mis. al 1721");
      Auto a1 = new Auto("Jose","123");
      Auto a2 = new Auto("Pedro","456");
      Auto a3 = new Auto("Juan","789");
      e1.agregarAuto(a1, 0, 1);
      e1.agregarAuto(a2, 1, 1);
      e1.agregarAuto(a3, 2, 2);

      //Patentes Existentes
      System.out.println(e1.buscarPatente("458"));
      System.out.println(e1.buscarPatente("789"));
      //Patente Inexistente
      System.out.println(e1.buscarPatente("159"));
      
      System.out.println(e1.toString());
   }
}

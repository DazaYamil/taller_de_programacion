package POO.RepasoFinal.Ejercicio3;

public class Evento extends Recital {
   private String motivo;
   private String nombreContratante;
   private String diaEvento;

   public Evento(String nombreBanda, int dimF, String motivo, String nombreContratante, String diaEvento){
      super(nombreBanda, dimF);
      this.motivo = motivo;
      this.nombreContratante = nombreContratante;
      this.diaEvento = diaEvento;
   }

   public String getMotivo() {
      return motivo;
   }
   public String getNombreContratante() {
      return nombreContratante;
   }
   public String getDiaEvento() {
      return diaEvento;
   }

   @Override
   public String actuar() {
      String aux = "";
      if(this.getMotivo().equals("SHOW")){
         aux += " Recuerden Colaborar con " + this.getNombreContratante();
      }else if(this.getMotivo().equals("SHOW TV")){
         aux += "Saludos Amigos Televidentes";
      }else if(this.getMotivo().equals("SHOW PRIVADO")){
         aux += "Un feliz cumpleaños para " + this.getNombreContratante();
      }
      aux += super.actuar();
      return aux;
   }
   
}

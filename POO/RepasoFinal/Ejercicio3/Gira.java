package POO.RepasoFinal.Ejercicio3;

public class Gira extends Recital{
   private String nombre;
   private Fecha [] fechas;
   private int fechaActual;
   private int dimF;
   private int dimL = 0;

   public Gira(String nombreBanda, int dimFTemas, String nombre, int dimF){
      super(nombreBanda, dimFTemas);
      this.nombre = nombre;
      this.dimF = dimF;
      this.fechas = new Fecha[dimF];
      this.fechaActual = -1;
   }

   public String getNombre() {
      return this.nombre;
   }
   public int getDimF() {
      return dimF;
   }
   public Fecha[] getFechas() {
      return this.fechas;
   }

   //Agregar Fecha
   public void agregarFecha(Fecha unaFecha){
      this.fechas[this.dimL] = unaFecha;
      this.fechaActual = dimL;
      this.dimL++;
   }

   //Actuar
   @Override
   public String actuar(){
      String aux = "Buenas Noches " + this.fechas[this.fechaActual].getCiudad();
      aux += super.actuar();
      return aux;
   }

}

package POO.RepasoFinal.Ejercicio3;

public class Fecha {
   private String ciudad;
   private String dia;

   public Fecha(String ciudad, String dia){
      this.ciudad = ciudad;
      this.dia = dia;
   }

   public String getCiudad() {
      return ciudad;
   }
   public String getDia() {
      return dia;
   }
   
}

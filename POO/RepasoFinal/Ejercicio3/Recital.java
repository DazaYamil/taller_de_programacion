package POO.RepasoFinal.Ejercicio3;

public abstract class Recital {
   private String nombreBanda;
   private String [] listaDeTemas;
   private int dimF;
   private int dimL = 0;

   public Recital(String nombreBanda, int dimF){
      this.nombreBanda = nombreBanda;
      this.dimF = dimF;
      this.listaDeTemas = new String[dimF];
      
   }

   public int getListaDeMusica() {
      return dimF;
   }
   public String[] getListaDeTemas() {
      return listaDeTemas;
   }
   public String getNombreBanda() {
      return nombreBanda;
   }

   //Agregar Tema
   public void agregarTema(String tema){
      this.listaDeTemas[this.dimL] = tema;
      this.dimL++;
   }

   //Actuar
   public String actuar(){
      String aux = "";
      for (int i = 0; i < this.dimL; i++) {
         aux += "\n Tema " + (i+1) + ": " +  this.listaDeTemas[i];      }
      return aux;
   }

}

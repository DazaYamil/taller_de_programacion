/* 5- El dueño de un restaurante entrevista a cinco clientes y les pide que califiquen (con puntaje de 1 a 10) los siguientes aspectos: (0) Atención al cliente (1) Calidad de la comida (2) Precio (3) Ambiente.
Escriba un programa que lea desde teclado las calificaciones de los cinco clientes para cada uno de los aspectos y almacene la información en una estructura. Luego imprima la calificación promedio obtenida por cada aspecto.
*/

package POO.Practica1;

import java.util.Random;

public class Ejercicio5 {
   public static void main(String[] args) {
      Random rd = new Random();

      int dimF = 5;
      int dimC = 4;
      int [][] calificacion = new int[dimF][dimC];

      for(int f = 0; f < dimF; f++){
         System.out.println("- Procesamos la calificacion del cliente nro " + (f+1)+ ":");
         for(int c = 0; c < dimC; c++){
            calificacion[f][c] = rd.nextInt(10);
         }
      }

      System.out.println(" Informacion de las calificaciones de los 5 clientes: ");
      for (int f = 0; f < dimF; f++) {
         for (int c = 0; c < dimC; c++) {
            System.out.print(calificacion[f][c] + " | ");
         }
         System.out.println();
      }
      System.out.println();
   }
}

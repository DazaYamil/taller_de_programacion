/* 4- Un edificio de oficinas está conformado por 4 pisos (1..8) y 3 oficinas por piso (1..4). Realice un programa que permita informar la cantidad de personas que concurrieron a cada oficina de cada piso. Para esto, simule la llegada de personas al edificio de la siguiente manera: a cada persona se le pide el nro. de piso y nro. de oficina a la cual quiere concurrir. La llegada de personas finaliza al indicar un nro. de piso 9. Al finalizar la llegada de personas, informe lo pedido.

*/

package POO.Practica1;
import java.util.Scanner;

public class Ejercicio4 {
   public static void main(String[] args) {
      Scanner entry = new Scanner(System.in);

      int dimF = 4;
      int dimC = 3;
      int [][] edificio = new int[dimF][dimC];

      int nroPiso = 0;
      int nroOficina = 0;
      System.out.println("Introduzca el Nro de Piso y Oficina que desea asistir: ");
      System.out.print(" - Nro Piso: ");
      nroPiso = entry.nextInt();
      System.out.print(" - Nro Oficina: ");
      nroOficina = entry.nextInt();

      while(nroPiso != 5){
         System.out.println();
         nroPiso--;
         nroOficina--;
         edificio[nroPiso][nroOficina]++;
         System.out.println("Introduzca el Nro de Piso y Oficina que desea asistir: ");
         System.out.print(" - Nro Piso: ");
         nroPiso = entry.nextInt();
         System.out.print(" - Nro Oficina: ");
         nroOficina = entry.nextInt();
      }

      // Mostrar en contenido de la matriz en consola
      System.out.println(" Mostrar los datos del Edificio por Consola: ");
      for (int f = 0; f < dimF; f++) {
         System.out.print("Piso nro " + (f+1) + ": ");
         for (int c = 0; c < dimC; c++) {
            System.out.print(edificio[f][c] + " | ");
         }
         System.out.println();
      }
      System.out.println();

      entry.close();
   }
}

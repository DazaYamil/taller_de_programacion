/* 2- Escriba un programa que lea las alturas de los 10 jugadores de un equipo de básquet y las almacene en un vector. Luego informe:
   - la altura promedio
   - la cantidad de jugadores con altura por encima del promedio 
*/

package POO.Practica1;
import java.util.Scanner;
import java.text.DecimalFormat;

public class Ejercicio2 {
   public static void main(String[] args) {
      Scanner entry = new Scanner(System.in);
      DecimalFormat df = new DecimalFormat("#.00");
      
      double [] jugadores = new double[10];
      double promedio = 0;
      double alturaPromedio = 0;
      int contador = 0;

      for(int i=0; i < jugadores.length ; i++){
         System.out.print("Introduzca la Altura del jugador " + (i+1) + ": ");
         jugadores[i] = entry.nextFloat();
         promedio += jugadores[i];
      }

      alturaPromedio = promedio / jugadores.length;

      System.out.println("A continuacion, mostraremos las alturas de todos los jugadores");
      for(int i=0; i < jugadores.length ; i++){
         System.out.println("Jugador " + (i+1) + " - Altura: " + df.format(jugadores[i]));
      }

      System.out.println();
      for(int i=0; i < jugadores.length ; i++){
         if(jugadores[i] > alturaPromedio){
            contador ++;
         }
      }
      System.out.println("La cantidad de Jugadores con Altura Superior a " + df.format(promedio) + " es de : " + contador);

      entry.close();
   }
}

/*3- Escriba un programa que defina una matriz de enteros de tamaño 5x5. Inicialice La matriz con números aleatorios entre 0 y 20.
Luego realice las siguientes operaciones:
- A) Mostrar el contenido de la matriz en consola.
- B) Calcular e informar la suma de los elementos de la fila 1
- C) Generar un vector de 5 posiciones donde cada posición j contiene la suma de los elementos de la columna j de la matriz. Luego, imprima el vector.
- D) Lea un valor entero e indique si se encuentra o no en la matriz. En caso de encontrarse indique su ubicación (fila y columna) en caso contrario imprima “No se encontró el elemento”
 */

package POO.Practica1;
import java.util.Random;

public class Ejercicio3 {
   public static void main(String[] args) {
      Random rd = new Random();
      
      int dimF = 5;
      int dimC = 5;
      int [][] matriz = new int[dimF][dimC];

      for(int f = 0; f < dimF; f++){
         for(int c = 0; c < dimC; c++){
            matriz[f][c] = rd.nextInt(21);
         }
      }

      // A. Mostrar en contenido de la matriz en consola
      System.out.println(" A. Mostrar los datos de la Matriz por Consola: ");
      for (int f = 0; f < dimF; f++) {
         for (int c = 0; c < dimC; c++) {
            System.out.print(matriz[f][c] + " | ");
         }
         System.out.println();
      }
      System.out.println();

      // B. Calcular e informar la suma de los elementos de la fila 1
      System.out.println(" B. Calcular e Informar la suma de los elementos de la Fila 1 :");
      int sumaFila1 = 0;
      for(int f = 0; f < dimF; f++){
         sumaFila1 += matriz[f][0];
      }
      System.out.println(" La suma total es de: " + sumaFila1);
      System.out.println();

      // C. Generar Vector e Informar
      System.out.println(" C. Generar Vector e Informar");
      int [] vector = new int[5];
      int contador;
      for (int c = 0; c < dimC; c++) {
         contador = 0;
         for (int f = 0; f < dimF; f++) {
            contador += matriz[f][c];
         }
         vector[c] = contador;
      }
      for(int i = 0; i < vector.length; i++){
         System.out.println("- Posicion " + (i+1) + ": " + vector[i]);
      }
      System.out.println();

      // D. Buscar e informar Numero X
      boolean seguir = false;
      int f = 0;
      int c = 0;
      int valorX = 14;

      while((f < dimF && c < dimC) && !seguir){
         if(matriz[f][c] == valorX){
            seguir = true;
         }else{
            c++;
         }

         if(c == 5){
            c = 0;
            f++;
         }
      }

      if(seguir){
         //Se encontro el valor en la matriz
         System.out.println("Se encontro el Valor " + valorX + " en la posicion -> Fila: " + (f + 1) + " - Columna: " + (c + 1));   
      }else{
         //No se encontro el valor en la matriz
         System.out.println("No se encontro el Valor " + valorX + " en la Matriz.");
      }
   }
}

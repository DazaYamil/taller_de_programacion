program orden;
const
	dimF=5;
type
	vector = array[1..dimF] of integer;
	
	procedure ordenarVector(var v:vector);
	var
		i,j,actual:integer;
	begin
		for i:= 2 to dimF do begin //Recorro el vector desde su componente 2 hasta su dimL
			actual:=v[i]; // en la variable actual almaceno el valor del componente i del vector este primer caso actual:=7;
			j:= i - 1; // en la variable de iteracion le asignamos el valor de i menos 1, en este primer caso le asignamos j:=1;
			while(j > 0)and(v[j] > actual)do begin //mientras j sea mayor a 0(es decir no llegue al final del arreglo) y comparo valores
				v[j + 1]:=v[j]; // almaceno el valor del componente menor un paso hacia adelante
				j:= j - 1; // decremento en 1 el iterador j 
			end;
			v[j + 1]:=actual; //almaceno en el componente correcto en valor de la variable actual
		end;
	end;
	
	
	procedure mostrarVector(v:vector);
	var
		i:integer;
	begin
		for i:=1 to dimF do 
			write(v[i],'.  ');
	end;
	
var
	v:vector;
begin
	
	v[1]:= 15;
	v[2]:= 7;
	v[3]:= 1;
	v[4]:= 45;
	v[5]:= 38;

	//	sin orden -> 15. 7. 1. 45. 38
	//  orden de menor a mayor -> 1. 5. 7. 38. 45
	//  orden de mayor a menor -> 45. 38. 7. 5. 1
	
	ordenarVector(v);
	mostrarVector(v);

end.

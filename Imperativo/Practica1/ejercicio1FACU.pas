{Implementar un programa que procese la información de las ventas de productos de un comercio (como máximo 20). 
De cada venta se conoce código del producto (entre 1 y 15) y cantidad vendida (como máximo 99 unidades). 
El ingreso de las ventas finaliza con el código 0 (no se procesa).

a. Almacenar la información de las ventas en un vector. El código debe generarse automáticamente (random) y la cantidad se debe leer. 
b. Mostrar el contenido del vector resultante.
c. Ordenar el vector de ventas por código.
d. Mostrar el contenido del vector resultante.
e. Eliminar del vector ordenado las ventas con código de producto entre dos valores que se ingresan como parámetros. 
f. Mostrar el contenido del vector resultante.
g. Generar una lista ordenada por código de producto de menor a mayor a partir del vector resultante del inciso e., sólo para los códigos pares.
h. Mostrar la lista resultante.}

program Clase1MI;
const 
	dimF = 20; //La cantidad maxima del vector
type 
	rangoCodigo = 0..15;
    rangoCantVentas = 1..99;
    rangoVector = 0..dimF;
    
    ventas = record //Registro de las ventas
			codigoP: rangoCodigo; 
			cantVendidas: rangoCantVentas;
	end;
	
	vector = array [rangoVector] of ventas; //Vector de 20 componentes que en cada componente almacena una venta(codigoP, cantVendida)
	 
	lista = ^nodo;
	nodo = record
	          dato: ventas;
	          sig: lista;
	        end;

	//PROCESO 1 -> CARGARVECTOR
	procedure cargarVector (var v: vector; var dimL: rangoVector);
  
		//PROCESO 1.A -> LEERVENTA
		procedure leerVenta (var unaVenta:ventas);
		begin
			Randomize; //BUSCAR INFORMACION -> YAMIL*
			write('Codigo de producto: ');
			unaVenta.codigoP := Random(15); //Esto nos genera un codigo aleatorio de un intervalo de 0 a 15
			writeln (unaVenta.codigoP); //Esto nos muestra el numero que genera del random
			
			if (unaVenta.codigoP <> 0)then begin
				write ('Ingrese cantidad (entre 1 y 99): ');
				readln (unaVenta.cantVendidas);
			end;
		end;

	var 
		unaVenta: ventas;
	begin
		dimL := 0;
		leerVenta (unaVenta); //PROCESO 1.A
		while (unaVenta.codigoP <> 0)  and ( dimL < dimF ) do begin
			dimL := dimL + 1;
			v[dimL]:= unaVenta;
			LeerVenta (unaVenta); //PROCESO 1.A
		end;
	end;

	procedure imprimirVector (v: vector; dimL: rangoVector); //PROCESO 2
	var
		i: integer;
	begin
		write ('         -');
		for i:= 1 to dimL do
         write ('-----');
		writeln;
		write ('  Codigo:| ');
		for i:= 1 to dimL do begin
			if(v[i].codigoP <= 9)then
				write ('0');
			write(v[i].codigoP, ' | ');
		end;
		writeln;
		writeln;
		write ('Cantidad:| ');
		for i:= 1 to dimL do begin
			if (v[i].cantVendidas <= 9)then
				write ('0');
			write(v[i].cantVendidas, ' | ');
		end;
		writeln;
		write ('         -');
		for i:= 1 to dimL do
			write ('-----');
		writeln;
		writeln;
	end;

	//PROCESO 3 -> ORDENARVECTOR
	procedure ordenarVector (var v: vector; dimL: rangoVector);
	var 
		i, j, pos: rangoVector; 
		item: ventas; //item almacena la posicion del codigo menor del campo registro
	begin
		for i:= 1 to dimL - 1 do begin {busca el mínimo y guarda en pos la posición}
			pos := i;
			for j := i+1 to dimL do 
				if (v[j].codigoP < v[pos].codigoP)then 
					pos:=j;
					
			{intercambia v[i] y v[pos]}
			item := v[pos];   
			v[pos] := v[i];   
			v[i] := item;
		end;
	end;
	
	
procedure Eliminar (var v: vector; var dimL: rangoVector; valorInferior, valorSuperior: rangoCodigo);

  function BuscarPosicionInferior (v: vector; dimL: rangoVector; valorInferior: rangoCodigo): rangoVector;
  var
    pos: rangoVector;
  begin
    pos:= 1;
    while (pos <= dimL) and (valorInferior > v[pos].codigoP) do
       pos:= pos + 1;
    if (pos > dimL) then
        BuscarPosicion:= 0;
      else 
        BuscarPosicion:= pos;
  end;
  
  function BuscarPosicionSuperior (v: vector; dimL, posInferior : integer; elemABuscar: rangoCodigo): rangoVector;
  begin
    while (pos <= dimL) and (elemABuscar >= v[pos].codigoP) do
       pos:= pos + 1;
    if (pos > dimL) then BuscarPosicionDesde:= dimL
                    else BuscarPosicionDesde:= pos - 1;
  end;

var
  posInferior, posSuperior, salto, i: rangoVector; 
Begin
  posInferior:= BuscarPosicionInferior (v, dimL, valorInferior);//DEVUELVE POSICION INFERIOR
  if (posInferior <> 0)  then begin
     posSuperior:= BuscarPosicionSuperior (v, dimL, posInferior, valorSuperior) ; //devuelve pos superior
         
         //Escribir el código correspondiente para hacer el corrimiento y disminuir la dimensión lógica
         
       end;
end;

procedure GenerarLista (v: vector; dimL: rango3; var L: lista);

  procedure AgregarAdelante (var L: lista; elem: venta);
  begin
    //Completar
  end;
  
  function Cumple (num: rango1): boolean;
  begin
    //Completar
   end;
  
var i: rango3; 
begin
  L:= nil;
  for i:= dimL downto 1 do 
    if Cumple (v[i].codigoP) then AgregarAdelante (L, v[i]);
end; 

procedure ImprimirLista (L: lista);
begin
 //Completar 
end;
}


var 
	v: vector; //Variable del vector
    dimL: rangoVector;
    //valorInferior, valorSuperior: rango1;
    //L: lista;
    
Begin
	cargarVector (v, dimL); //PROCESO 1
	writeln;
	if (dimL = 0) then //comprueba si esta vacio
       writeln ('--- Vector sin elementos ---')
	else begin //si tiene elementos...
       writeln ('--- Vector ingresado --->');
       writeln;
       imprimirVector (v, dimL); //PROCESO 2.(imprimir)                
       writeln;
       
       writeln ('--- Vector ordenado --->');
       writeln;
       ordenarVector (v, dimL); //PROCESO 3
       ImprimirVector (v, dimL); //PROCESO 2
       
       
       //PEDIR PRIMER PARAMETRO>>INFERIOR
       write ('Ingrese valor inferior: ');
       readln (valorInferior);
       //PEDIR SEGUNDO PARAMETRO>>SUPERIOR
       write ('Ingrese valor superior: ');
       readln (valorSuperior); 
       
       //PROCESO 4. /PUNTO E-
       Eliminar (v, dimL, valorInferior, valorSuperior);
       if (dimL = 0) then
           writeln ('--- Vector sin elementos, luego de la eliminacion ---')
	   else begin
           writeln;
           writeln ('--- Vector luego de la eliminacion --->');
           writeln;
           ImprimirVector (v, dimL);
           
           GenerarLista (v, dimL, L);
           if (L = nil) then //comprueba si esta vacia la lista
               writeln ('--- Lista sin elementos ---')
           else begin
               writeln;
               writeln ('--- Lista obtenida --->');
               writeln;
               ImprimirLista (L); 
             end;
       end;
    end;
                       
end.

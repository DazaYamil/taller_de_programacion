program ejercicio;
const
	dimF = 8; //Cantidad maxima del vector
type
	rangeGender = 1..8; //rango de generos 
	rangeVector = 0..dimF; //rango del vector
	
	//registro de pelicula
	movies = record
		codeMovie:integer;
		codeGender:rangeGender;
		score:real;
	end;
	
	//lista donde va a contener las peliculas por genero
	list = ^nodo;
	nodo = record
		data:movies;
		sig:list;
	end;
	
	movieMax = record
		codeMovie:integer;
		score:real;
	end;
	
	vectorList = array [rangeGender] of list; //vector de 8 componentes, que cada componente contiene una lista, y la lista tiene un registro.
	vectorCount = array [rangeGender] of integer; //Vector de 8 componentes, que cada componente contiene un numero X 
	vectorGenderMax = array [rangeGender] of movieMax; // Vector nuevo que contiene por cada genero(1..8) el codigo de pelicula con mayor puntaje
		
	//PROCESS 1 - SET VECTOR COUNT
	procedure setVectorCount(var vc:vectorCount);
	var
		i:rangeGender;
	begin
		//inicializar el vector contador en 0 en todas sus posiciones
		for i:= 1 to dimF do
			vc[i]:=0;
	end;
		
	//PROCESS 2 - SET VECTOR
	procedure setVector(var v:vectorList);
	var
		i:rangeVector;
	begin
		for i:= 1 to dimF do begin
			v[i] := nil; //inicializamos todas las posiciones del vector(que son una lista) en nil
		end;
	end;
	
	//PROCESO 3.A - READ MOVIES
	procedure readMovie(var m:movies);
	begin
		//Randomize;
		writeln('Movie:');
		write(' - Enter movie code: '); readln(m.codeMovie);
		if(m.codeMovie <> -1)then begin
			//m.codeGender:=random(8);
			write(' - Gender code: '); readln(m.codeGender);
			write(' - Movie Score: '); readln(m.score);
			writeln();
		end
		else
			writeln('charging is complete'); //la carga a finalizado....
	end;
	
	//PROCESS 3.B - ADD FORTH
	procedure addForth(var l:list; dataMovie:movies);
	var
		nue:list;
	begin
		new(nue);
		nue^.data:=dataMovie;
		nue^.sig:=l;
		l:=nue;
	end; 
	
	//PROCESS 3 - LOAD VECTOR
	procedure loadVector(var v:vectorList; var vc:vectorCount);
	var
		dataMovie:movies;
	begin
		readMovie(dataMovie); //PROCESS TWO.A
		while (dataMovie.codeMovie <> -1) do begin
			addForth(v[dataMovie.codeGender],dataMovie); //PROCESS TWO.B
			vc[dataMovie.codeGender] := vc[dataMovie.codeGender] + 1;
			readMovie(dataMovie); //PROCESS TWO.A
		end;
	end;
	
	
	//PROCESS 4 - LOAD VECTOR NEW
	procedure loadVectorNew(v:vectorList; var vg:vectorGenderMax);
	var
		i:rangeGender;
		maxScore:real;
		maxCode:integer;
	begin
		// setVectorGender(vg); //PROCESO 4.A -> inicializo el vector con todas sus posiciones en 0
		for i:= 1 to dimF do begin
			maxScore :=-1;
			maxCode :=-1;
			while(v[i] <> nil)do begin 
				if(v[i]^.data.score > maxScore)then begin
					maxScore:=v[i]^.data.score;
					maxCode:=v[i]^.data.codeMovie;
					vg[i].codeMovie:=maxCode;
					vg[i].score:=maxScore;
				end;
				v[i]:=v[i]^.sig;
			end;
		end;
	end;
	
	//PROCESS 5 - SHOW VECTOR
	procedure showVector(v:vectorList; vc:vectorCount);
		//PROCESS 5.A - SHOW LIST
		procedure showList(l:list);
		var
			i:integer;
		begin
			i:=1;
			while(l <> nil)do begin
				writeln(i,'.Movie Code: ',l^.data.codeMovie,'  |  Gender Code: ',l^.data.codeGender,'  |  Movie Score: ',l^.data.score:2:2);
				l:=l^.sig;
				i:=i + 1;
			end;
		end;
	var
		i:rangeVector;
	begin
		writeln();
		for i:=1 to dimF do begin
			writeln('- VECTOR ',i,' have ',vc[i],'  movies : ');
			showList(v[i]); //PROCESS 4.A
			writeln();
			writeln();
		end;
	end;
	
	//PROCESS 6 - SHOW VECTOR NEW
	procedure showVectorNew(vg:vectorGenderMax);
	var
		i:rangeGender;
	begin
		for i:= 1 to dimF do begin
			case i of 
				1 : writeln('*Genero 1. Accion -> El codigo de pelicula que genero mayor puntuacion es: ',vg[i].codeMovie,' con ',vg[i].score:2:2);
				2 : writeln('*Genero 2. Aventura -> El codigo de pelicula que genero mayor puntuacion es: ',vg[i].codeMovie,' con ',vg[i].score:2:2);
				3 : writeln('*Genero 3. Drama -> El codigo de pelicula que genero mayor puntuacion es: ',vg[i].codeMovie,' con ',vg[i].score:2:2);
				4 : writeln('*Genero 4. Suspenso -> El codigo de pelicula que genero mayor puntuacion es: ',vg[i].codeMovie,' con ',vg[i].score:2:2);
				5 : writeln('*Genero 5. Comedia -> El codigo de pelicula que genero mayor puntuacion es: ',vg[i].codeMovie,' con ',vg[i].score:2:2);
				6 : writeln('*Genero 6. Belica -> El codigo de pelicula que genero mayor puntuacion es: ',vg[i].codeMovie,' con ',vg[i].score:2:2);
				7 : writeln('*Genero 7. Documental -> El codigo de pelicula que genero mayor puntuacion es: ',vg[i].codeMovie,' con ',vg[i].score:2:2);
				8 : writeln('*Genero 8. Terror -> El codigo de pelicula que genero mayor puntuacion es: ',vg[i].codeMovie,' con ',vg[i].score:2:2);
			end;
		end;
	end;
	
	//PROCESS 7 - SORT VECTOR NEW
	procedure sortVectorNew(var vg:vectorGenderMax);
	var
		i, j: rangeVector;
		current:real;
	begin
		for i:= 2 to dimF do begin
			current:=vg[i].score;
			j:= i - 1;
			while(j > 0) AND (vg[j].score > current ) do begin
				vg[j + 1]:=vg[j];
				j:= j - 1;
			end;
			vg[j + 1].score:=current;
		end;
	end;
	
	//PROCESS 8 - SHOW VECTOR SORT
	procedure showVectorSort(vg:vectorGenderMax);
	var
		i:rangeVector;
	begin
		writeln();
		writeln(' El Vector Ordenado por puntuacion de menor a mayor, quedo de la siguiente manera: ');
		writeln();
		for i:= 1 to dimF do 
			writeln(i,' Code Movie: ',vg[i].codeMovie,' with: ',vg[i].score:2:2,' of score.');
	end;
	
	//PROCESS 9 - SHOW VECTOR MAX MIN
	procedure showVectorMaxMin(vg:vectorGenderMax);
	begin
		writeln();
		writeln('The movie with high score is -> code:  ',vg[dimF].codeMovie,' with score of: ',vg[dimF].score:2:2);
		writeln('The movie with lesser score is -> code:  ',vg[1].codeMovie,' with score of: ',vg[1].score:2:2);
		writeln();
	end;
	
var
	v:vectorList;
	vc:vectorCount;
	vg:vectorGenderMax;
begin
	setVectorCount(vc); //PROCESS 1 - inicializo el vector contador
	setVector(v); //PROCESS 2 - inicializo el vector de listas

	loadVector(v,vc); //PROCESS 3 - cargo el vector de listas
	loadVectorNew(v,vg); //PROCESS 4 - cargo el nuevo vector 

	showVector(v,vc); //PROCESS 5 - imprimo el vector de listas
	showVectorNew(vg); //PROCESS 6 - mostrar como quedo el vector donde almacena por genero el codigo de pelicula con mayor puntuacion
	
	sortVectorNew(vg); //PROCESS 7 - ordenar el vector nuevo con el metodo de insercion
	showVectorSort(vg); //PROCESS 8 - mostrar como quedo el vector ordenado
	
	showVectorMaxMin(vg); //PROCESS 9 - mostrar el vector maximo(codigo de pelicula) y minimo(codigo de pelicula)
end.


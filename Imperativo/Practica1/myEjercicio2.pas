program ejercicio2;
const
	dimF = 10;
type
	rangoVector = 0..dimF;
	
	oficinas = record
		codigo:integer;
		dniPropietario:integer;
		valor:real;
	end;

	vectorOficinas = array[rangoVector] of oficinas;
	
	//PROCESO 1 - CARGAR VECTOR
	procedure cargarVector(var v:vectorOficinas; var dimL:rangoVector);
		//PROCESO 1.A - LEER OFICINA
		procedure leerOficina(var dato:oficinas);
		begin
			write('- Ingrese el codigo de IDE: '); readln(dato.codigo);
			if(dato.codigo <> -1)then begin
				write('- DNI del propietario: '); readln(dato.dniPropietario);
				write('- Valor de expensa: '); readln(dato.valor);
				writeln();
			end
			else
				writeln(' --------- La carga a finalizado ----------');
		end;
	var
		datoOfi : oficinas;
	begin
		dimL:=0;
		leerOficina(datoOfi); //PROCESO 1.A
		while(dimL < dimF) AND (datoOfi.codigo <> -1)do begin
			dimL:= dimL + 1;
			v[dimL]:=datoOfi;
			leerOficina(datoOfi); //PROCESO 1.A
		end;
	end;
	
	//PROCESO 2.A ORDEN POR SELECCION
	procedure ordenarPorSeleccion(var v:vectorOficinas; var dimL:rangoVector);
	var
		i,j,pos:rangoVector; 
		item:integer;
	begin
		for i:= 1 to dimL - 1 do begin
			pos:= i;
			for j:= i + 1 to dimL do
				if(v[j].codigo < v[pos].codigo)then
					pos:=j;
			item:=v[pos].codigo;
			v[pos]:=v[i];
			v[i].codigo:=item;
		end;
	end;
	
	//PROCESO 2.B ORDEN POR INSERCION
	procedure ordenarPorInsercion(var v:vectorOficinas; var dimL:rangoVector);
	var
		i, j: rangoVector;
		actual: integer;
	begin
		for i:= 2 to dimL do begin
			actual:=v[i].codigo;
			j:= i - 1;
			while(j > 0) AND (v[j].codigo >  actual) do begin
				v[j + 1]:=v[j];
				j:=j - 1;
			end;
			v[j + 1].codigo := actual;
		end;
	end;
	
	//PROCESO 2 ORDENAR VECTOR
	procedure ordenarVector(var v:vectorOficinas; var dimL:rangoVector);
	var
		opcion:char;
	begin
		writeln('A- METODO SELECCION');
		writeln('B- METODO INSERCION');
		write('¿Con que metodo desea ordenar el vector? _'); readln(opcion);
		if(opcion = 'a')then begin
			ordenarPorSeleccion(v,dimL); //PROCESO 2.A
			writeln('Metodo seleccion.....');
		end
		else if(opcion = 'b')then begin
			ordenarPorInsercion(v,dimL); //PROCESO 2.B
			writeln('Metodo insercion.....');
		end
		else 
			writeln('Opcion incorrecta.....');
	end;
	
	//PROCESO 3 IMPRIMIR VECTOR
	procedure imprimirVector(v:vectorOficinas; dimL:rangoVector);
	var
		i:rangoVector;
	begin
		for i:= 1 to dimL do 
			writeln('CODIGO IDE: ',v[i].codigo,'   |   DNI PROPIETARIO: ',v[i].dniPropietario, '   |   VALOR: ',v[i].valor:2:2);
	end;
	
	
var
	v:vectorOficinas;
	dimL:rangoVector;
begin
	cargarVector(v,dimL); //PROCESO 1
	ordenarVector(v,dimL); //PROCESO 2
	imprimirVector(v,dimL); //PROCESO 3
end.

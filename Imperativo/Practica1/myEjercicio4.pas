program ejercicio4;
const
	dimF = 8; //La cantidad maxima de componentes del vector Dimension Fisica
	dimFtwo = 3; //La cantidad maxima de componentes del vector nuevo a generar
type
	rangoVector = 1..dimF ; //El rango del vector es de 8 componentes
	rangoRubro = 1..8;
	rangoNuevo = 1..dimFtwo; 
	
	producto = record // producto es un registro con 3 campos(codigo,rubro,precio)
		codigo:integer;
		rubro:rangoRubro;
		precio:real;
	end;
	
	lista = ^nodo;
	nodo = record
		dato: producto;
		sig:lista;
	end;
	
	vectorRubros = array [rangoRubro] of lista;
	vectorRubro3 = array[rangoNuevo] of producto;
	
	//PROCESO 1.A - INICIALIZAR VECTOR
	procedure inicializarVector(var v:vectorRubros);
	var
		i:rangoVector;
	begin
		for i:= 1 to dimF do 
			v[i]:=nil;
	end;
	
	//PROCESO 1.B LEER PRODUCTO
	procedure leerProducto(var p:producto);
	begin
		write('Codigo: '); readln(p.codigo);
		write('Rubro(1 - 8): '); readln(p.rubro);
		write('Precio: $'); readln(p.precio);
		writeln();
	end;
	
	//PROCESO 1.C - AGREGAR ORDENADO
	procedure agregarOrdenado(var l:lista; p:producto);
	var
		ant,act,nue:lista;
	begin
		new(nue);
		nue^.dato:=p;
		ant:=l;
		act:=l;
		while(act <> nil) AND (p.codigo > act^.dato.codigo)do begin
			ant:=act;
			act:=act^.sig;
		end;
		if(act = ant)then
			l:=nue
		else
			ant^.sig:=nue;
		nue^.sig:=act
	end;
	
	//PROCESO 1 - CARGAR VECTOR
	procedure cargarVector(var v:vectorRubros);
	var 
		p:producto;
	begin
		inicializarVector(v); //PROCESO 1.A -> inicializar cada componente en NIL porque es una lista
		leerProducto(p); //PROCESO 1.B
		while(p.precio <> 0)do begin
			agregarOrdenado(v[p.rubro],p); //PROCESO 1.C
			leerProducto(p);
		end;
	end;
	
	//PROCESO 2.A - MOSTRAR LISTA
	procedure mostrarLista(l:lista);
	begin
		writeln(' - Codigo: ',l^.dato.codigo,'  -  Rubro: ',l^.dato.rubro,'  -  Precio: $',l^.dato.precio:2:0);
	end;
	
	//PROCESO 2- MOSTRAR VECTOR
	procedure mostrarVector(v:vectorRubros);
	var
		i:rangoVector;
	begin
		for i:= 1 to dimF do begin
			writeln('VECTOR ',i,' Contenido: ');
			while(v[i] <> nil)do begin
				mostrarLista(v[i]); //PROCESO N.A
				v[i]:=v[i]^.sig;
			end;
			writeln();
		end;
	end;
	
	//PROCESO 3 - CARGAR NUEVO VECTOR
	procedure cargarNuevoVector(v:vectorRubros; var v3:vectorRubro3);
	var 
		pos:integer;
	begin
		pos:=0;
		while(v[3] <> nil) AND (pos < dimFTwo)do begin //Recorro toda la lista del rubro 3
			pos:= pos + 1;
			v3[pos]:=v[3]^.dato;
			v[3]:=v[3]^.sig;
		end;
	end;
	
	//PROCESO 4 - MOSTRAR VECTOR NUEVO
	procedure mostrarVectorNuevo(v3:vectorRubro3);
	var
		i:rangoNuevo;
	begin
		for i:= 1 to dimFtwo do 
			writeln('Prod. ',i,': CODIGO  ',v3[i].codigo,'    PRECIO $ ',v3[i].precio:2:0);
		writeln();
	end;
	
	//PROCESO 5 - ORDENAR VECTOR
	procedure ordenarVectorNuevo(var v3:vectorRubro3);
	var
		i, j, pos : rangoNuevo;
		actual:producto;
	begin
		for i:= 1 to dimFtwo - 1 do begin
			pos:=i;
			for j:= i + 1 to dimFtwo do begin
				if(v3[j].precio < v3[pos].precio)then
					pos:=j
			end;
			actual:=v3[pos];
			v3[pos]:=v3[i];
			v3[i]:=actual;
		end;
	end;
	
var
	v:vectorRubros;
	v3:vectorRubro3;
begin
	cargarVector(v); //PROCESO 1
	mostrarVector(v); //PROCESO 2
	
	cargarNuevoVector(v,v3); //PROCESO 3
	writeln('Vector nuevo del rubro 3:');
	mostrarVectorNuevo(v3); //PROCESO 4
	
	ordenarVectorNuevo(v3); //PROCESO 5
	writeln('Vector Ordenado por precio:');
	mostrarVectorNuevo(v3); //PROCESO 4
	
end.

program ordenacion;
const
	dimF = 10; //Cantidad maxima de componentes de nuestro vector para usar
type
	rangoCodigo = 0..15; // 16 lugares
	rangoCantidad = 1..99; // 99 lugares
	rangoVector = 0..dimF; //11 lugares - CONSULTAR*****
	
	venta = record
		codigo : rangoCodigo;
		cantVendida : rangoCantidad;
	end;
	
	vectorVentas = array [rangoVector] of venta; //Vector de 10 componetes, que en cada uno de ellos almacena una venta
	
	lista = ^nodo;
	nodo = record
		dato : venta;
		sig : lista;
	end;
	
// ----------------------  Fin  de la declaracion de los TYPES  -------------------------------------
	
	
	//PROCESO 1 - CARGAR VECTOR
	procedure cargarVector(var v : vectorVentas; var dimL : rangoVector);
		//PROCESO 1.A - LEER VENTA DE UN PRODUCTO 
		procedure leerVenta(var ventaP : venta);
		begin
			Randomize; //Con esta instruccion podemos usar un metodo random, que nos brinda un numero entre un intervalo.
			ventaP.codigo:=random(16);
			writeln('Codigo generado: ',ventaP.codigo);
			if( ventaP.codigo <> 0)then begin
				write('Cantidad Vendida: '); 
				readln(ventaP.cantVendida);
			end;
		end;
		
	var
		ventaP : venta; 
	begin
		dimL := 0; //Inicializo la dimensión logica para llevar un control de los elementos del vector cargados
		leerVenta(ventaP); //PROCESO 1.A
		while(ventaP.codigo <> 0) AND (dimL < dimF)do begin
			dimL := dimL + 1;
			v[dimL] := ventaP;
			leerVenta(ventaP); //PROCESO 1.A
		end;
	end;
	
	
	//PROCESO 2 - IMPRIMIR VECTOR
	procedure imprimirVector (v: vectorVentas; dimL: rangoVector); 
	var
		i: integer;
	begin
		write ('         -');
		for i:= 1 to dimL do
		write ('-----');
		writeln;
		write ('  Codigo:| ');
		for i:= 1 to dimL do begin
			if(v[i].codigo <= 9)then
				write ('0');
			write(v[i].codigo, ' | ');
		end;
		writeln;
		writeln;
		write ('Cantidad:| ');
		for i:= 1 to dimL do begin
			if (v[i].cantVendida <= 9)then
				write ('0');
			write(v[i].cantVendida, ' | ');
		end;
		writeln;
		write ('         -');
		for i:= 1 to dimL do
			write ('-----');
		writeln;
		writeln;
	end;
	
	
	// PROCESO 3 -  ORDENAR VECTOR (metodo de seleccion);
	procedure ordenarVector(var v : vectorVentas; var dimL : rangoVector );
	var
		i, j, pos : rangoVector;
		item: rangoCodigo;
	begin
		for i := 1 to dimL - 1 do begin
			pos := i;
			for j := i + 1 to dimL do 
				if( v[j].codigo < v[pos].codigo )then
					pos := j;
			item := v[pos].codigo;
			v[pos] := v[i];
			v[i].codigo := item;
		end;
	end;
	
	//FUNCION 1 - BUSCAR POSICION INFERIOR
	function buscarPosicionInferior(v:vectorVentas; dimL:rangoVector; valorInferior:rangoCodigo ) : rangoVector;
	var
		pos: rangoVector;
	begin
		pos:=1;
		while(pos <= dimL) AND (valorInferior > v[pos].codigo )do
			pos:= pos + 1;
		if(pos > dimL)then
			buscarPosicionInferior := 0
		else
			buscarPosicionInferior := pos;
	end;
	
	//FUNCION 2 - BUSCAR POSICION SUPERIOR
	function buscarPosicionSuperior(v:vectorVentas; dimL,posicionInferior:rangoVector; valorSuperior:rangoCodigo) : rangoVector;
	var
		pos : rangoVector;
	begin
		pos := posicionInferior;
		while( pos <= dimL) AND (valorSuperior >= v[pos].codigo) do
			pos:=pos + 1;
		if(pos > dimL)then
			buscarPosicionSuperior := dimL
		else
			buscarPosicionSuperior := pos - 1;
	end;
	
	//PROCESO 4 - ELIMINAR VECTOR DEACUERDO A 2 VALORES
	procedure eliminarVector(
		var v : vectorVentas;
		var dimL : rangoVector;
		valorInferior,valorSuperior:rangoCodigo);
	var
		posicionInferior,posicionSuperior, i : rangoVector;
	begin
		posicionInferior := buscarPosicionInferior(v, dimL, valorInferior); //FUNCION 1 (retorna la posicion del valor inferior)
		if( posicionInferior <> 0)then begin
			posicionSuperior := buscarPosicionSuperior( v, dimL, posicionInferior, valorSuperior); //FUNCION 2
			for i:= posicionSuperior downto posicionInferior do begin
				v[i] := v[i + 1];
				dimL := dimL - 1;
			end;
		end
		else
			writeln('No se puede eliminar el vector con los valores enviados....');
	end;
	
	
	
	//PROCESO 5 - GENERAR LISTA ORDENADA
	procedure generarListaOrdenada(v:vectorVentas; dimL:rangoVector; var l:lista);
		//PROCESO 5.A - AGREGAR ATRAS
		procedure agregarAtras(var l,ult:lista; dato:venta);
		var
			nue:lista;
		begin
			new(nue);
			nue^.dato:=dato;
			nue^.sig:=nil;
			if(l = nil)then
				l:=nue
			else
				ult^.sig:=nue;
			ult:=nue;
		end;
	var
		ult:lista;
		i:rangoVector;
		dato:venta;
	begin
		for i:= 1 to dimL do begin
			if(v[i].codigo MOD 2 = 0)then begin
				dato.codigo:=v[i].codigo;
				dato.cantVendida:=v[i].cantVendida;
				agregarAtras(l,ult,dato); //PROCESO 5.A
			end;
		end;
	end;
	
	
	//PROCESO 6 - IMPRIMIR LISTA 
	procedure imprimirLista(l:lista);
	begin
		while(l^.sig <> nil)do begin
			writeln('- Codigo: ',l^.dato.codigo, ' |  - Cantidad Vendidad: ',l^.dato.cantVendida);
			l:=l^.sig;
		end;
	end;
	
	
	
var
	v : vectorVentas; 
	dimL : rangoVector;
	valorInferior, valorSuperior : rangoCodigo;
	l:lista;
	
begin
	l:=nil; //Inicializo la lista 
	cargarVector(v,dimL); //PROCESO 1
	writeln();
	
	if(dimL = 0)then
		writeln('El vector se encuentra sin elementos...')
	else begin
		writeln(' ------- Vector cargado -------');
		writeln();
		imprimirVector(v,dimL); //PROCESO 2
		writeln();
		
		ordenarVector(v,dimL); //PROCESO 3
		
		writeln(' ------- Vector Ordenado por Codigo(de menor a mayor) -------');
		writeln();
		imprimirVector(v,dimL); //PROCESO 2
		writeln();
		
		write('Ingrese el valor inferior(0 a 15): '); readln(valorInferior);
		write('Ingrese el valor superior(0 a 15): '); readln(valorSuperior);
		writeln();
		eliminarVector(v, dimL, valorInferior, valorSuperior); //PROCESO 4
		
		if(dimL = 0)then
			writeln(' ------- Vector vacio despues de la eliminación deacuerdo a 2 valores ----------')
		else begin
			writeln('--------- Vector despues del proceso Eliminar -----------');
			writeln();
			imprimirVector(v,dimL);
			writeln();
			
			generarListaOrdenada(v,dimL,l); //PROCESO 5
			if(l = nil)then //verifico si la lista esta vacia
				writeln('------------- Lista sin elementos --------------')
			else begin
				writeln(' ------------- Lista generada -----------------');
				writeln();
				imprimirLista(l); //PROCESO 6
			end;	
		end;
	end;
	
	
end.

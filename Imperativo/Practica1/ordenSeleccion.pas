program ejercicio;
const
	dimF = 5;
type
	vector = array [1..dimF] of integer;

	procedure ordenarVector(var v:vector);
	var
		i,j,pos:integer;
		item:integer;
	begin
		for i:= 1 to dimF -1 do begin //recorremos el vector hasta el anteultimo componente
			pos:=i; //en la variable auxiliar POS almacenamos el valor de la iteracion
			for j:= i + 1 to dimF do //recorremos el vector desde la segunda posicion hasta el final buscando el minimo
				if(v[j] > v[pos])then //consulto si lo que contiene el vector en la posicion J es menor a los que contiene el vector en la posicion POS
					pos:=j; // en la variable POS almacenamos la posicion del componente minimo
			item:=v[pos]; // item se va a cargar con el valor minimo del arreglo -> v[POS]
			v[pos]:=v[i]; // intercambio de valores v[pos] <-> v[i]. es decir en la posicion minima encontrada almacenamos el valor que tenemos en nuestro primer elemento. dep de la ite**
			v[i]:=item; // ahora almacenamos el minimo dependiendo de la vuelta de iteracion que estemos
		end;
	end;

	procedure mostrarVector(v:vector);
	var
		i:integer;
	begin
		for i:=1 to dimF do 
			write(v[i],'.  ');
	end;

var
	v:vector;
begin
	v[1]:= 23;
	v[2]:= 10;
	v[3]:= 4;
	v[4]:= 42;
	v[5]:= 1;

	//	sin orden -> 23. 10. 4. 42. 1
	//  orden de menor a mayor -> 1. 4. 10. 23. 42
	//  orden de mayor a menor -> 42. 23. 10. 4. 1
	
	ordenarVector(v);
	mostrarVector(v);
end.

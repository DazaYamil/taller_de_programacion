program recursividad;
type
	lista = ^nodo;
	nodo = record
		dato: integer;
		sig:lista;
	end;
	
	//PROCESO 1.A - AGREGAR ATRAS
	procedure agregarAtras(var l,ult:lista; num:integer);
	var
		nue:lista;
	begin
		new(nue); //creamos un nuevo nodo para el dato a guardar
		nue^.dato:=num; //guardamos el dato en la variable nue campo dato
		nue^.sig:=nil;
		//writeln('entramos con el numero ',num);
		if(l = nil)then
			l:=nue
		else
			ult^.sig:=nue;
		ult:=nue;
	end;
	
	//PROCESO 1.B - AGREGAR ADELANTE
	procedure agregarAdelante(var l:lista; num:integer);
	var
		nue:lista;
	begin
		new(nue);
		nue^.dato:=num;
		nue^.sig:=l;
		l:=nue;
	end;
	
	//PROCESO 1.A
	procedure cargarLista(var l:lista);
	var
		num:integer;
	begin
		{Randomize;
		num:=random(5); //Ejercicio es 100, pero a modo de prueba 5
		write('Dato: ',num); readln();}
		write('Ingrese numero: '); readln(num);
		if(num <> 0)then begin
			cargarLista(l);
			//agregarAtras(l,ult,num);// PROCESO 1.A
			agregarAdelante(l,num);
			//writeln('agregamos ',num);
		end
		else
			l:=nil
	end;
	
	//PROCESO 2 - IMPRIMIR VECTOR
	procedure imprimirLista(l:lista);
	begin
		writeln();
		writeln(' ------ Contenido de la lista ----- ');
		while(l <> nil)do begin
			writeln('Dato: ',l^.dato);
			l:=l^.sig;
		end;
		writeln();
	end;
	
	//FUNCTION 1 - MIN VALOR - Lista 7 1 5 nil 
	function minValor(l:lista ; aux:integer):integer;
	begin
		if(l = nil)then 
			minValor:=aux
		else begin
			if(l^.dato < aux)then
				aux:=l^.dato;
			l:=l^.sig;
			minValor:=minValor(l,aux);
		end;
	end;
	
	//FUNCTION 2 - MAX VALOR
	function maxValor(l:lista ; aux2:integer):integer;
	begin
		if(l = nil)then 
			maxValor:=aux2
		else begin
			if(l^.dato > aux2)then
				aux2:=l^.dato;
			l:=l^.sig;
			maxValor:=maxValor(l,aux2);
		end;
	end;

	//FUNCTION BUSCAR
	function buscar(l:lista; x:integer):boolean;
	begin
		if (l = nil)then
			buscar:=false
		else
			{if(l^.dato = x)then
				buscar:=true
			else
				buscar:= buscar(l^.sig,x);}
			buscar:=( x= l^.dato ) or ( buscar( l^.sig,x )); //ESTUDIAR ******
	end;
	
	
var
	l:lista;
	min,aux:integer;
	max,aux2,x:integer;	
begin
	cargarLista(l); //PROCESO 1 - cargarLista
	imprimirLista(l); //PROCESO 2 - imprimirLista
	
	aux:=999;
	aux2:=-1;
	min:=minValor(l,aux); //FUNCTION 
	max:=maxValor(l,aux2); //FUNCTION 
	writeln('El valor minimo de la lista es: ',min);
	writeln('El valor maximo de la lista es: ',max);
	
	write('Ingrese el dato a buscar: '); readln(x);
	if(buscar(l,x))then
		writeln('El dato ',x,' se encontro en la lista')
	else
		writeln('El dato no esta en la lista');
end.

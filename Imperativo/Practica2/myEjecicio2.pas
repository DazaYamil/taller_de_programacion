program recursividad;

	//PROCESS 1 - IMPRIMIR NUM
	procedure imprimirNum(num:integer);
	var
		aux:integer;
	begin
		if (num <> 0) then begin
			aux:=num MOD 10;
			imprimirNum(num DIV 10);
			write(aux,' | ');
		end;
	end;
	
var
	num:integer;
begin //program principal
	write('Ingresa un numero: '); readln(num);
	while(num <> 0)do begin
		write('Los digitos del ',num,' es: ');
		imprimirNum(num);
		writeln();
		write('Ingresa un numero: '); readln(num);
	end;
end.

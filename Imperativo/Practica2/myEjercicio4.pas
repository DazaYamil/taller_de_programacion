program recursividad;
const
	dimF = 5;
type
	rangoVector = 1..dimF;
	vector = array [rangoVector] of integer;

	//PROCESO 1 - CARGAR VECTOR
	procedure cargarVector(var v:vector; dimL:integer);
	var
		num:integer;
	begin
		//num:=random(10);
		write('Dato: '); readln(num);
		if(dimL < dimF)then begin
			dimL:=dimL + 1;
			v[dimL]:=num;
			cargarVector(v,dimL);
		end;
	end;
	
	//FUNCION BUSCAR MAX - INCISO B
	{function buscarMax(v:vector; dimL:integer; max:integer) : integer;
	begin
		if(dimL < dimF)then begin// 1 7 3
			if(v[dimL] > max)then
				max:=v[dimL];
			buscarMax:=buscarMax(v,dimL,max);
			dimL:=dimL + 1;
		end
		else
			buscarMax:=max;
	end;}
	
	//FUNCTION SUMA VECTOR
	function sumaVector(v:vector; dimL:integer):integer;
	begin
		if(dimL < dimF)then begin
			dimL:=dimL + 1;
			sumaVector:=sumaVector(v,dimL) + v[dimL];
		end
		else
			sumaVector:=0;
	end;

var
	v:vector;
	dimL:integer;
	//max:integer;
begin
	Randomize;
	dimL:=0;
	cargarVector(v,dimL); //PROCESO 1
	
	//max:=-1;
	//writeln('El valor maximo del vector es: ',buscarMax(v,dimL + 1,max));
	writeln('La suma de los elementos del vector es: ',sumaVector(v,dimL));

end.

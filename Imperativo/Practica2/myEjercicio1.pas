program recursividad;
const
	dimF = 10; 
type
	rangoVector = 0..dimF; 
	vector = array [rangoVector] of char;
	
	//PROCESO 1 - MODULO A
	procedure moduloA(var v:vector; var dimL:rangoVector);
	var
		c:char;
	begin
		write('Caracter: '); readln(c);
		if(dimL < dimF)AND(c <> '.')then begin
			dimL:=dimL + 1;
			v[dimL]:=c;
			moduloA(v,dimL);
		end;
	end;
		
	//PROCESO 2 - IMPRIMIR VECTOR
	procedure	imprimirVector(v:vector; dimL:rangoVector);
	var
		i:rangoVector;
	begin
		writeln();
		writeln(' ----- Contenido del Vector ------');
		for i:= 1 to dimL do 
			writeln(i,'- Contenido: ',v[i]);
	end;
	
var
	v:vector;
	dimL:rangoVector;
begin
	dimL:=0;
	moduloA(v,dimL); //PROCESO 1
	imprimirVector(v,dimL); //PROCESO 2

end.

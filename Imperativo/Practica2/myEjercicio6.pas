{6.- Realizar un programa que lea números y que utilice un procedimiento recursivo que escriba el equivalente en binario de un número decimal. El programa termina cuando el usuario ingresa el número 0 (cero). 
Ayuda:  Analizando las posibilidades encontramos que: Binario (N) es N si el valor es menor a 2. ¿Cómo obtenemos los dígitos que componen al número? ¿Cómo achicamos el número para la próxima llamada recursiva? Ejemplo: si se ingresa 23, el programa debe mostrar: 10111.}

program recursividad;
type
	
	//FUNCTION - MOSTRAR BINARIO
	function mostrarBinario(num:integer):string;
	var
		cadena:string;
	begin
		cadena:='';
		while(num <> 0)then begin
			cadena:=cadena + (num MOD 2)
			num:=num DIV 2;
		end;
		mostrarBinario:=cadena;
	end;

	//PROCESO 1 - CARGAR NUMEROS
	procedure	cargarNumeros();
	var
		num:integer;
	begin
		write('Ingresa un numero: '); readln(num);
		if(num <> 0)then
			writeln('El numero binario de ',num,' es: ',mostrarBinario(num));
			cargarNumeros();
	end;

begin
	cargarNumeros();
end.

asd

asd

program recursividad;

	//FUNCTION - MOSTRAR BINARIO
	function mostrarBinario(num:integer):string;
	var
		dig:integer;
		cadena:string;
	begin
	    cadena:='';
		while(num > 0)do begin
			dig:= num mod 2;
			num:=num DIV 2;
			cadena:=char(dig+48) + cadena;
		end;
		mostrarBinario:=cadena;
	end;

	//PROCESO 1 - CARGAR NUMEROS
	procedure	cargarNumeros();
	var
		num:integer;
	begin
		write('Ingresa un numero: '); readln(num);
		if(num <> 0)then
			writeln('El numero binario de ',num,' es: ',mostrarBinario(num));
			cargarNumeros();
	end;

begin
	cargarNumeros();
end.

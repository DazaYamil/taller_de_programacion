program parcial;
const
	maxRubro = 3; //ENUNCIADO 10
type
	rangoRubro = 1..maxRubro;
	producto = record
		codigo:integer;
		rubro:rangoRubro;
		stock:integer;
		precio:real;
	end;

	arbol = ^nodo;
	nodo = record
		dato:producto;
		HD:arbol;
		HI:arbol;
	end;
	
	vector = array [rangoRubro] of arbol;
	
	//PROCESO 1.A
	procedure inicializarArbol(var v:vector);
	var
		i:rangoRubro;
	begin
		for i:= 1 to maxRubro do 
			v[i]:=nil;
	end;
	
	//PROCESO 1.B
	procedure leerProducto(var p:producto);
	begin
		with p do begin
			write('Codigo de producto: '); readln(codigo);
			if(codigo <> -1)then begin
				write('Rubro: '); readln(rubro);
				write('Stock: '); readln(stock);
				write('Precio: '); readln(precio);
				writeln();
			end
			else begin
				writeln();
				writeln(' ----- HEMOS TERMINADO LA CARGA ----- ');
				writeln();
			end;
		end;
	end;
	
	//PROCESO 1.C
	procedure cargarArbol(var a:arbol; p:producto);
	begin
		if(a = nil)then begin
			new(a);
			a^.dato:=p;
			a^.HI:=nil;
			a^.HD:=nil;
		end
		else
			if(p.codigo < a^.dato.codigo)
				then cargarArbol(a^.HI, p)
				else cargarArbol(a^.HD, p);
	end;
	
	//PROCESO 1
	procedure almacenarArbol(var v:vector);
	var
		p:producto;
	begin
		inicializarArbol(v);//PROCESO 1.A
		leerProducto(p);//PROCESO 1.B
		while(p.codigo <> -1)do begin
			cargarArbol(v[p.rubro], p); //PROCESO 1.C
			leerProducto(p);
		end;
	end;
	
	//PROCESO 2
	procedure busqueda(a:arbol; codigo:integer; var esta:boolean);
	begin
		if(a <> nil)then begin
			if(a^.dato.codigo = codigo)
				then esta:=true
				else
					if(codigo > a^.dato.codigo)
						then busqueda(a^.HD, codigo, esta)
						else busqueda(a^.HI, codigo, esta);		
		end;
	end;
	
	procedure procesoB(v:vector);
	var
		rubro:rangoRubro;
		codigo:integer;
		esta:boolean;
	begin
		write('En que rubro desea buscar: '); readln(rubro);
		write('Que codigo desea buscar: '); readln(codigo);
		esta:=false;
		busqueda(v[rubro],codigo,esta); //PROCESO 2.A
		
		if(esta)
			then writeln('El producto de codigo ',codigo,' del rubro ',rubro,' Se encuentro...')
			else writeln('El producto de codigo ',codigo,' del rubro ',rubro,' No se encuentro...')
			
	end;
	
	//PROCESO 3.A //SI QUEREMOS PARA MAS EFICIENCIA PODEMOS CREAR OTRO REGISTRO CON 2 CAMPOS
	procedure buscarMaximo(a:arbol; var p:producto);
	begin
		if(a^.HD = nil)
			then p:=a^.dato
			else buscarMaximo(a^.HD, p);
	end;
	
	//PROCESO 3
	procedure procesoC(v:vector);
	var
		i:rangoRubro;
		p:producto;
	begin
		for i:=1 to maxRubro do begin
			if(v[i] <> nil)then begin
				buscarMaximo(v[i],p); //PROCESO 3.A
				writeln('Codigo: ',p.codigo,' | stock: ',p.stock);
			end
			else begin
				writeln();
				writeln('Arbol del rubro ',i,' Se encuentra vacio :C');
				writeln();
			end;
		end;
	end;
	
	//PROCESO 4.A
	procedure contarArbol(a:arbol; code1,code2:integer; var contador:integer);
	begin
		if(a <> nil)then begin
			if((a^.dato.codigo > code1)AND(a^.dato.codigo < code2))then begin
				contarArbol(a^.HD, code1, code2, contador);
				contador:=contador + 1;
				contarArbol(a^.HI, code1, code2, contador);
			end
			else
				if(a^.dato.codigo < code1)then
					contarArbol(a^.HD, code1, code2, contador)
				else
					contarArbol(a^.HI, code1, code2, contador);
		end;
	end;
	
	//PROCESO 4
	procedure procesoD(v:vector);
	var
		code1,code2:integer;
		contador:integer;
		i:rangoRubro;
	begin
		write('Ingrese el codigo inferior: '); readln(code1);
		write('Ingrese el codigo superior: '); readln(code2);
		writeln();
		
		for i:= 1 to maxRubro do begin
			contador:=0;
			contarArbol(v[i],code1,code2,contador); //PROCESO 4.A
			writeln('La cantidad de codigos que existen entre ',code1,' y ',code2,' del rubro ',i,' es: ',contador);
		end;
	end;

var
	v:vector;
begin
	almacenarArbol(v); //PROCESO 1
	procesoB(v); //PROCESO 2
	procesoC(v); //PROCESO 3
	procesoD(v); //PROCESO 4
end.

program parcial;
const 
	dimF = 10;
type
	rangoVector = 0..dimF;
	
	oficina = record
		codigo:integer;
		dni:integer;
		expensa:real;
	end;
	
	vector = array[rangoVector] of oficina;
	
	//PROCESO 1.A
	procedure leerOficina(var f:oficina);
	begin
		with f do begin
			write('Codigo de identificación: '); readln(codigo);
			if(codigo <> -1)then begin
				write('Dni del propietario: '); readln(dni);
				write('Valor de expensa: $'); readln(expensa);
				writeln(); //salto de linea
			end
			else begin
				writeln();
				writeln(' --- Hemos terminado la carga --- ');
				writeln();
			end;
		end;
	end;
	
	//PROCESO 1
	procedure cargarVector(var v:vector; var dimL:rangoVector);
	var
		f:oficina;
	begin
		dimL:=0;
		leerOficina(f); //PROCESO 1.A
		while((dimL < dimF) AND (f.codigo <> -1))do begin
			dimL:=dimL + 1;
			v[dimL]:=f;
			leerOficina(f);
		end;
	end;
	
	//PROCESO 2
	procedure imprimirVector(v:vector; dimL:rangoVector);
	var
		i:rangoVector;
	begin
		for i:= 1 to dimL do begin
			writeln('Oficina ',i,':');
			writeln(' -Codigo IDE: ',v[i].codigo,' | DNI: ',v[i].dni,' | Valor: $',v[i].expensa:2:0);
			writeln();
		end;
		writeln();
	end;
	
	//PROCESO 3
	procedure ordenarVector(var v:vector; dimL:rangoVector);
	var
		i,j:rangoVector;
		actual:oficina;
	begin
		for i:= 2 to dimL do begin
			actual:=v[i];
			j:=i - 1;
			while((j > 0)AND(v[j].codigo > actual.codigo))do begin
				v[j + 1]:=v[j];
				j:=j - 1;
			end;
			v[j + 1]:=actual;
		end;
	end;
	
	//PROCESO 4.A
	procedure buscar(v:vector; dimL:rangoVector; buscarCodigo:integer; var esta:boolean; var indice:rangoVector);
	var
		pri,ult,medio:rangoVector;
	begin
		esta:=false;
		indice:=0;
		pri:=1; ult:=dimL; medio:=(pri + ult) div 2;
		while((pri <= ult) AND (buscarCodigo <> v[medio].codigo))do begin
			if(buscarCodigo < v[medio].codigo)then
				ult:=medio - 1
			else
				pri:=medio + 1;
			medio:=(pri + ult) div 2;
		end;
		if((pri <= ult)AND(buscarCodigo = v[medio].codigo))then begin
			esta:=true;
			indice:=medio;
		end;
	end;
	
	//PROCESO 4
	procedure busquedaDicotomica(v:vector; dimL:rangoVector);
	var
		buscarCodigo:integer;
		indice:rangoVector;
		esta:boolean;
	begin
		write('Ingrese el codigo de identificacion que quiere buscar: '); readln(buscarCodigo);
		buscar(v,dimL,buscarCodigo,esta,indice); //PROCESO 4.A
		if(esta)then begin
			writeln(' ----- El dato a buscar se encuentra dentro del vector ----- ');
			writeln('DNI del propietario: ',v[indice].dni);
		end
		else
			writeln(' ---- El codigo no se encuentra dentro del vector ----');
	end;
	
	//FUNCTION 1
	function sumaExpensas(v:vector; dimL:rangoVector; seguir:integer):real;
	begin
		if(seguir < dimL)then begin
			seguir:=seguir + 1;
			sumaExpensas:=sumaExpensas(v,dimL,seguir) + v[seguir].expensa;
		end;
	end;
	
var
	v:vector;
	dimL:rangoVector;
	seguir:integer;
begin
	cargarVector(v,dimL); //PROCESO 1
	writeln(' ----- VECTOR SIN ORDEN ------');
	imprimirVector(v,dimL); //PROCESO 2
	
	ordenarVector(v,dimL); //PROCESO 3
	writeln(' ----- VECTOR CON ORDEN ------ ');
	imprimirVector(v,dimL);
	
	busquedaDicotomica(v,dimL); //PROCESO 4
	seguir:=0;
	writeln('La suma total de todas las expensas es de: $',sumaExpensas(v,dimL,seguir):2:0); //FUNCTION 1

end.

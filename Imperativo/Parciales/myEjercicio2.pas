program parcial;
type
	rangoAnio = 2010..2018;
	cadena=string[20];
	auto = record
		patente:integer;
		anioFabricacion:rangoAnio;
		marca:cadena;
		modelo:cadena;
	end;
	arbol = ^nodo;
	nodo = record
		dato:auto;
		HI:arbol;
		HD:arbol;
	end;
	
	lista = ^nodoLista;
	nodoLista = record
		dato:auto;
		sig:lista;
	end;
	vector = array[rangoAnio] of lista;
	
	//PROCESO 1.A
	procedure leerAuto(var dato:auto);
	begin
		with dato do begin
			write('Patente: '); readln(patente);
			if(patente <> 0)then begin
				write('Anio de fabricacion: '); readln(anioFabricacion);
				write('Marca: '); readln(marca);
				write('Modelo: '); readln(modelo);
				writeln();
			end;
		end;
	end;
	
	//PROCESO 1.B
	procedure cargarArbol(var a:arbol; dato:auto);
	begin
		if(a = nil)then begin
			new(a);
			a^.dato:=dato;
			a^.HI:=nil;
			a^.HD:=nil;
		end
		else
			if(dato.patente > a^.dato.patente)
				then cargarArbol(a^.HD, dato)
				else cargarArbol(a^.HI, dato);
	end;
	
	//PROCESO 1
	procedure almacenarArbol(var a:arbol);
	var
		dato:auto;
	begin
		a:=nil;
		leerAuto(dato); //PROCESO 1.A
		while(dato.patente <> 0)do begin
			cargarArbol(a,dato); //PROCESO 1.B
			leerAuto(dato);
		end;
	end;
	
	//PROCESO 2.A
	procedure imprimirDatos(auto:auto);
	begin
		writeln(' - Marca: ',auto.marca,' | Patente: ',auto.patente,' | Modelo: ',auto.modelo,' | Anio de fabricacion: ',auto.anioFabricacion);
	end;
	
	//PROCESO 2
	procedure mostrarArbol(a:arbol);
	begin
		if(a <> nil)then begin
			mostrarArbol(a^.HI);
			imprimirDatos(a^.dato); //PROCESO 2.A
			mostrarArbol(a^.HD);
		end;
	end;
	
	//FUNCTION 1  ford audi ford toyota fb3ford fiat fiat
	function contarMarca(a:arbol; marca:cadena):integer;
	begin
		if(a <> nil)then begin
			if(a^.dato.marca = marca)then
				contarMarca:= contarMarca(a^.HI,marca) + contarMarca(a^.HD,marca) + 1
			else
				contarMarca:= contarMarca(a^.HI,marca) + contarMarca(a^.HD,marca) + 0;
		end
		else
			contarMarca:=0;
	end;
	
	//PROCESO 3.A
	procedure inicializarListas(var v:vector);
	var
		i:rangoAnio;
	begin
		for i:= 2010 to 2018 do
			v[i]:=nil;
	end;
	
	//PROCESO 3.B.A
	procedure agregarLista(var l:lista; dato:auto);
	var
		nue:lista;
	begin
		new(nue);
		nue^.dato:=dato;
		nue^.sig:=l;
		l:=nue;
	end;
	
	//PROCESO 3.B
	procedure recorrerYcargar(a:arbol; var v:vector);
	begin
		if(a <> nil)then begin
			agregarLista(v[a^.dato.anioFabricacion],a^.dato); //PROCESO 3.B.A
			recorrerYcargar(a^.HI, v);
			recorrerYcargar(a^.HD, v);
		end;
	end;
	
	//PROCESO 3.C.A
	procedure mostrarDato(l:lista);
	begin
		if(l <> nil)then begin
			writeln('Anio de fabricacion: ',l^.dato.anioFabricacion,' | Patente: ',l^.dato.patente,' | Marca: ',l^.dato.marca);
			mostrarDato(l^.sig);
		end;
	end;
	
	//PROCESO 3.C
	procedure imprimirVector(var v:vector);
	var
		i:rangoAnio;
	begin
		for i:= 2010 to 2018 do begin
			if(v[i] <> nil)then begin
				writeln('---- Autos con el Anio ',i,' ----');
				mostrarDato(v[i]);//PROCESO 3.C.A
				writeln();
			end
			else begin
				writeln();
				writeln('---- Autos con el anio ',i,' SIN STOCK ----');
				writeln();
			end;
		end;
	end;
	
	//PROCESO 3
	procedure cargarVector(a:arbol; var v:vector);
	begin
		inicializarListas(v); //PROCESO 3.A
		recorrerYcargar(a,v); //PROCESO 3.B
		imprimirVector(v); //PROCESO 3.C
	end;
	
	//PROCESO 4.A
	procedure busqueda(a:arbol; patente:integer; var esta:boolean; var au:auto);
	begin
		if(a <> nil)then begin
			if(a^.dato.patente = patente)then begin
				esta:=true;
				au:=a^.dato;
			end
			else
				if(patente > a^.dato.patente)
					then busqueda(a^.HD, patente, esta, au)
					else busqueda(a^.HI, patente, esta, au);		
		end;
	end;
	
	//PROCESO 4
	procedure buscarPatente(a:arbol);
	var
		patente:integer;
		au:auto;
		esta:boolean;
	begin
		write('Que patente desea buscar: '); readln(patente);
		esta:=false;
		busqueda(a,patente,esta,au); //PROCESO 4.A
		if(esta)then begin
			writeln('La patente ',patente,' se encontro: ');
			writeln('Datos -> Marca: ',au.marca,' | Anio de fabricacion: ',au.anioFabricacion,' | modelo: ',au.modelo);
		end
		else begin
			writeln();
			writeln('La patente ',patente,' No se encontro :c');
			writeln();
		end;
	end;
	
	

var
	a:arbol;
	marca:cadena;
	v:vector;
begin
	almacenarArbol(a); //PROCESO 1
	mostrarArbol(a); //PROCESO 2
	
	write('Ingrese la marca para saber el stock: '); readln(marca);
	writeln('La cantidad de autos de la marca ',marca,' es ',contarMarca(a,marca)); //FUNCTION 1
	
	cargarVector(a,v); //PROCESO 3
	buscarPatente(a); //PROCESO 4
end.


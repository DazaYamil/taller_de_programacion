program parcial;
const
	diaMax = 3;
type
	rangoDia = 1..diaMax;
	entrada = record
		dia:rangoDia;
		codigo:integer;
		asiento:integer;
		monto:real;
	end;
	
	lista = ^nodo;
	nodo = record
		dato:entrada;
		sig:lista
	end;
	
	contarEntradas = record
		codigo:integer;
		cantidad:integer;
	end;
	
	lista2 = ^nodo2;
	nodo2 = record
		dato:contarEntradas;
		sig:lista2;
	end;
	
	vectorListas = array [rangoDia] of lista;
	
	//PROCESO 1.A
	procedure inicializarListas(var v:vectorListas);
	var
		i:rangoDia;
	begin
		for i:= 1 to diaMax do 
			v[i]:=nil;
	end;
	
	//PROCESO 1.B
	procedure leerEntrada(var e:entrada);
	begin
		with e do begin
			write('Codigo de la entrada: '); readln(codigo);
			if(codigo <> 0)then begin
				write('Dia: '); readln(dia);
				write('Asiento: '); readln(asiento);
				write('Monto: '); readln(monto);
				writeln();
			end
			else begin
				writeln();
				writeln(' - Hemos terminado la carga - ');
				writeln();
			end;
		end;
	end;
	
	//PROCESO 1.C
	procedure agregarOrdenado(var l:lista; e:entrada);
	var
		ant,act,nue:lista;
	begin
		new(nue);
		nue^.dato:=e;
		ant:=l;
		act:=l;
		while((act <> nil) AND (act^.dato.codigo > e.codigo))do begin
			ant:=act;
			act:=act^.sig;
		end;
		if(ant = act)then
			l:=nue
		else 
			ant^.sig:=nue;
		nue^.sig:=act;
	end;

	//PROCESO 1
	procedure cargarVector(var v:vectorListas);
	var
		e:entrada;
	begin
		inicializarListas(v); //PROCESO 1.A
		leerEntrada(e); //PROCESO 1.B
		while(e.codigo <> 0)do begin
			agregarOrdenado(v[e.dia],e); //PROCESO 1.C
			leerEntrada(e);
		end;
	end;

    //PROCESO 2.A
    procedure buscarMinimo(var v:vectorListas; var minEntrada:contarEntradas);
    var
		i:rangoDia;
		indice:integer;
    begin
        indice:=0; //guarda el indice donde sacamos el minimo del vector para avanzar
        minEntrada.codigo:=9999;
        for i:= 1 to diaMax do begin //recorremos cada lista
            if(v[i] <> nil)then begin //mientras la lista i no este en nil
                if(v[i]^.dato.codigo < minEntrada.codigo)then begin //proceso de actualizar
                    indice:=i;
                    minEntrada.codigo:=v[i]^.dato.codigo; //guardo el dato minimo
                end;
            end;
        end;

        if(minEntrada.codigo <> 9999)then 
            v[indice]:=v[indice]^.sig; //avanzo al siguiente nodo de la lista que encontre el minimo
    end;

    //PROCESO 2.B
    procedure agregarAtras(var l,ult:lista2; dato:contarEntradas);
    var
        nue:lista2;
    begin
        new(nue);
        nue^.dato:=dato;
        nue^.sig:=nil;
        if(l = nil)then 
            l:=nue
        else    
            ult^.sig:=nue;
        ult:=nue;
    end;
	
	//PROCESO 2
	procedure procesoB(v:vectorListas; var l:lista2);
    var
        minEntrada,actualEntrada:contarEntradas;
        contador:integer;
        ult:lista2;
    begin
        l:=nil; //inicializo nuestra nueva lista en nil, para poder guardar datos
        buscarMinimo(v,minEntrada); //PROCESO 2.A 
        while(minEntrada.codigo <> 9999)do begin //mientras 
            contador:=0; //inicializo el contador en 0 para comenzar cada codigo nuevo
            actualEntrada := minEntrada; //guardo en una variable auxiliar el codigo de ese momento
            while((minEntrada.codigo <> 9999) AND (actualEntrada.codigo = minEntrada.codigo))do begin //mientras no sea 9999 y siga leyendo el mismo codigo, hacemos:
                contador:=contador + 1; //incrementamos el contador
                buscarMinimo(v,minEntrada); //buscamos el minimo
            end;
            actualEntrada.cantidad:=contador;
            agregarAtras(l,ult,actualEntrada); //PROCESO 2.B - agregamos a la lista nueva de manera ordenada con agregarAtras
        end;
    end;

    //PROCESO 3.A
    procedure mostrarDatos(dato:contarEntradas);
    begin
        writeln(' * CODIGO DE ENTRADA ',dato.codigo,', Tiene ',dato.cantidad,' de entradas vendidas * ');
    end;

    //PROCESO 3
    procedure procesoD(l:lista2);
    begin
        if(l <> nil)then begin
            mostrarDatos(l^.dato); //PROCESO 3.A
            procesoD(l^.sig);
        end;
    end;

var
	v:vectorListas;
	newLista:lista2;
begin
	cargarVector(v); //PROCESO 1
	procesoB(v,newLista); //PROCESO 2 - Merge acumulador
    procesoD(newLista); //PROCESO 3 - Mostrar la nueva lista
end.

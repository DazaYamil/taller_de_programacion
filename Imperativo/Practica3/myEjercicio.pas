// Practica sobre arboles....
{ Los arboles no deben tener un ciclo, es decir que son elementos conectados entre si.
 Los arboles tiene un raiz y esa raiz no puede tener un padre pero si cada nodo. 
}
program arboles;
type
	arbol = ^nodo;
	nodo = record
		dato:integer;
		sigHI:arbol;
		sigHD:arbol;
	end;
	
	//PROCESO 1 - cargarArbol
	procedure cargarArbol(var a:arbol; num:integer);
	begin
		if( a = nil)then begin//Si nuestro arbol esta vacio, creamos la raiz
			new(a); //PASO IMPORTANTE - Crear espacio en memoria para el nodo
			a^.dato := num; //Asignamos la raiz de nuestro arbol con el primer dato ingresado
			//Inicializamos los hijos de la raiz o mejor dicho sus nodos.
			a^.sigHI:=nil; //Hijo izquierdo(NODO) le asignamos nil
			a^.sigHD:=nil; //Hijo derecho(NODO) le asignamos nil
		end
		else
			if(num > a^.dato)then //Si el dato a ingresar el mayor a la raiz:
				cargarArbol(a^.sigHD, num) //lo almacenamos en el nodo derecho HIJO
			else
				cargarArbol(a^.sigHI, num); //SINO: lo almacenamos en el nodo izquierdo HIJO
	end;
	
	//PROCESO 2 - imprimirArbol
	procedure imprimirArbol(a:arbol);
	begin
		if(a <> nil)then begin
			imprimirArbol(a^.sigHI);
			writeln(a^.dato);
			imprimirArbol(a^.sigHD);
		end;
	end;
	
	//FUNCTION 1 - buscarNum
	function buscarNum(a:arbol; num:integer):arbol;
	begin
		if(a = nil)then
			buscarNum:=nil
		else
			if(num = a^.dato)then
				buscarNum:=a
			else begin
				if(num > a^.dato)then
					buscarNum:=buscarNum(a^.sigHD, num)
				else
					buscarNum:=buscarNum(a^.sigHI, num);
			end;
	end;

var
	a:arbol;
	num:integer;
	numBuscar:integer;
begin
	a:=nil;
	write('Ingresa un numero: '); readln(num);
	while(num <> 0)do begin
		cargarArbol(a,num); //Proceso 1
		write('Ingresa un numero: '); readln(num);
	end;
	imprimirArbol(a); //Proceso 2
	
	//numBuscar:=43;
	writeln();
	write('Que numero quiere buscar: '); readln(numBuscar);
	if(buscarNum(a,numBuscar) = nil)then
		writeln('El numero ',numBuscar,' No esta dentro del arbol')
	else
		writeln('El numero ',numBuscar,' Esta dentro del arbol y se encontro correctamente...');
	
end.

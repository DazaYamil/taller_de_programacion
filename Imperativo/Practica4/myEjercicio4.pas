program merge4;
const
	maxFuncion = 7;
type
	rangoFuncion = 1..maxFuncion;
	entrada = record
		dia:rangoFuncion;
		codigoObra:integer;
		asiento:integer;
		precio:real;
	end;
	
	infoXobra= record
		codigoObra:integer;
		cantidad:integer;
	end;
	
	lista = ^nodo;
	nodo = record
		dato:entrada;
		sig:lista;
	end;
	
	lista2 = ^nodo2;
	nodo2 = record
		dato:infoXobra;
		sig:lista2;
	end;
	
	vectorListas = array [rangoFuncion] of lista; //Vector de 7 listas 

	//PROCESO 1.A
	procedure inicializarListas(var v:vectorListas);
	var
		i:rangoFuncion;
	begin
		for i:= 1 to maxFuncion do 
			v[i]:=nil;
	end;

	//PROCESO 1.B
	procedure leerEntrada(var e:entrada);
	begin
		with e do begin
			write('Codigo de Obra: '); readln(codigoObra);
			if(codigoObra <> 0)then begin
				write('Dia: '); readln(dia);
				write('Nro de Asiento: '); readln(asiento);
				write('Precio: $'); readln(precio);
				writeln();
			end;
		end;
	end;
	
	//PROCESO 1.C
	procedure agregarOrdenado(var l:lista; e:entrada);
	var
		ant,act,nue:lista;
	begin
		new(nue);
		nue^.dato:=e;
		ant:=l;
		act:=l;
		while((act <> nil) AND (act^.dato.codigoObra < e.codigoObra))do begin
			act:=act;
			act:=act^.sig;
		end;
		if(ant = act)
			then l:=nue
			else ant^.sig:=nue;
		nue^.sig:=act;
	end;
	
	//PROCESO 1
	procedure cargarVector(var v:vectorListas);
	var
		e:entrada;
	begin
		inicializarListas(v); //PROCESO 1.A
		leerEntrada(e); //PROCESO 1.B
		while(e.codigoObra <> 0)do begin
			agregarOrdenado(v[e.dia],e); //PROCESO 1.C
			leerEntrada(e);
		end;
	end;
	
	//PROCESO 2.A
	procedure buscarMinimo(var v:vectorListas; var minEntrada:entrada);
	var
		indice,i:rangoFuncion;
	begin
		indice:=0;
		minEntrada.codigoObra:=9999;
		for i:= 1 to maxFuncion do begin
			if(v[i] <> nil)then begin
				if(v[i]^.dato.codigoObra <= minEntrada.codigoObra)then begin
					indice:=i;
					minEntrada:=v[i]^.dato;
				end;
			end;
		end;
		if(minEntrada.codigoObra <> 9999)then
			v[indice]:=v[indice]^.sig;
	end;
	
	//PROCESO 2.B
	procedure agregarAtras(var l,ult:lista2; actual:infoXobra);
	var
		nue:lista2;
	begin
		new(nue);
		nue^.dato:=actual;
		nue^.sig:=nil;
		if(l = nil)	
			then l:=nue
			else ult^.sig:=nue;
		ult:=nue;
	end;
	
	//PROCESO 2
	procedure mergeAcumulador(v:vectorListas; var l:lista2);
	var
		ult:lista2;
		actual:infoXobra;
		minEntrada:entrada;
		contador:integer;
	begin
		l:=nil;
		buscarMinimo(v,minEntrada); //PROCESO 2.A
		while(minEntrada.codigoObra <> 9999)do begin
			actual.codigoObra:=minEntrada.codigoObra;
			contador:=0;
			while((minEntrada.codigoObra <> 9999) AND (minEntrada.codigoObra = actual.codigoObra))do begin
				contador:=contador + 1;
				buscarMinimo(v,minEntrada);
			end;
			actual.cantidad:=contador;
			agregarAtras(l,ult,actual);
		end;
		
	end;

	//PROCESO 3
	procedure mostrarMerge(l:lista2);
	begin
		if(l <> nil)then begin
			writeln('Para el codigo de obra ',l^.dato.codigoObra,' Tiene un total de ',l^.dato.cantidad,' de entradas vendidas.');
			mostrarMerge(l^.sig);
		end;
	end;

var
	v:vectorListas;
	l:lista2;
begin
	cargarVector(v); //PROCESO 1
	mergeAcumulador(v,l); //PROCESO 2
	mostrarMerge(l); //PROCESO 3
end.	

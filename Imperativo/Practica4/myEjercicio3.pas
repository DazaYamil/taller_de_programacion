program merge3;
const
	maxSucursal = 4;
type
	rangoSucursal = 0..maxSucursal;
	
	dias = 1..30;
	meses = 1..12;
	fecha = record
		dia:dias;
		mes:meses;
	end;
	venta = record
		fechaVenta:fecha;
		codProducto:integer;
		codSucursal:rangoSucursal;
		cantidadVendida:real;
	end;
	
	lista = ^nodo;
	nodo = record
		dato:venta;
		sig:lista;
	end;
	
	vectorListas = array [rangoSucursal] of lista;  
	
	//PROCESO 1.A
	procedure inicializarVector(var v:vectorListas);
	var
		i:rangoSucursal;
	begin
		for i:= 1 to maxSucursal do 
			v[i]:=nil;
	end;
	
	//PROCESO 1.B.A
	procedure leerFecha(var f:fecha);
	begin
		write('Dia de la venta: '); readln(f.dia);
		write('Mes de la venta: '); readln(f.mes);
	end;
	
	//PROCESO 1.B
	procedure leerVenta(var d:venta);
	var
		f:fecha;
	begin
		write('Cod de sucursal: '); readln(d.codSucursal);
		if(d.codSucursal <> 0)then begin
			write('Cod de Producto: '); readln(d.codProducto);
			leerFecha(f); //PROCESO 1.B.A
			write('Cantidad Vendida: '); readln(d.cantidadVendida);
			writeln();
		end;
	end;
	
	//PROCESO 1.C
	procedure agregarOrdenado(var l:lista; dato:venta);
	var
		ant,act,nue:lista;
	begin
		new(nue);
		nue^.dato:=dato;
		ant:=l;
		act:=l;
		while((act <> nil) AND (act^.dato.codProducto < dato.codProducto))do begin
			ant:=act;
			act:=act^.sig;
		end;
		if(ant = act)
			then l:=nue
			else ant^.sig:=nue;
		nue^.sig:=act;
	end;
	
	//PROCESO 1
	procedure cargarVector(var v:vectorListas);
	var
		dato:venta;
	begin
		inicializarVector(v); //PROCESO 1.A
		leerVenta(dato); //PROCESO 1.B
		while(dato.codSucursal <> 0)do begin
			agregarOrdenado(v[dato.codSucursal],dato);//PROCESO 1.C
			leerVenta(dato);
		end;
	end;
	
	//PROCESO 2.A
	procedure buscarMinimo(var v:vectorListas; var minVenta:venta; var monto:real);
	var
		indice,i:rangoSucursal;
	begin
		minVenta.codProducto:=9999;
		for i:= 1 to maxSucursal do begin
			if(v[i] <> nil)then begin
				if(v[i]^.dato.codProducto <= minVenta.codProducto)then begin
					//monto:=v[i]^.dato.cantidadVendida; PRIMERA OPCION
					minVenta:=v[i]^.dato;
					indice:=i;
				end;
			end;
		end;
		if(minVenta.codProducto <> 9999)then begin
			monto:=v[indice]^.dato.cantidadVendida; //SEGUNDA OPCION
			v[indice]:=v[indice]^.sig;
		end;
	end;
	
	//PROCESO 2.B
	procedure agregarAtras(var l,ult:lista; actual:venta);
	var
		nue:lista;
	begin
		new(nue);
		nue^.dato:= actual;
		nue^.sig:= nil;
		if(l = nil)
			then l:=nue
			else ult^.sig:=nue;
		ult:=nue;
	end;
	
	//PROCESO 2
	procedure mergeAcumulador(v:vectorListas; var l:lista);
	var
		ult:lista;
		minVenta,actual:venta;
		montoTotal,monto:real;
	begin
		l:=nil;
		monto:=0;
		buscarMinimo(v,minVenta,monto); //PROCESO 2.A
		while(minVenta.codProducto <> 9999)do begin
			actual:=minVenta;
			montoTotal:=0;
			while((minVenta.codProducto <> 9999) AND (minVenta.codProducto = actual.codProducto))do begin
				montoTotal:=montoTotal + monto;
				buscarMinimo(v,minVenta,monto);
			end;
			actual.cantidadVendida:=montoTotal;
			agregarAtras(l,ult,actual); //PROCESO 2.B
		end;
	end;
	
	//PROCESO 3
	procedure mostrarMerge(l:lista);
	begin
		if(l <> nil)then begin
			writeln('Cod de Producto: ',l^.dato.codProducto,' | cantidad vendida: $',l^.dato.cantidadVendida:2:2); 
			mostrarMerge(l^.sig);
		end;
	end;
 
var
	v:vectorListas;
	nueLista:lista;
begin
	cargarVector(v); //PROCESO 1
	mergeAcumulador(v,nueLista); //PROCESO 2
	mostrarMerge(nueLista); //PROCESO 3
end.

program merge2;
const
	maxGenero = 4;
type
	rangoGenero = 1..maxGenero;
	pelicula = record
		codPelicula:integer;
		codGenero:rangoGenero;
		puntaje:real;
	end;
	
	lista = ^nodo;
	nodo = record
		dato:pelicula;
		sig:lista;
	end;
	
	vectorPeliculas = array [rangoGenero] of lista;
	
	//PROCESO 1.A
	procedure inicializarVector(var v:vectorPeliculas);
	var
		i:rangoGenero;
	begin
		for i:= 1 to maxGenero do 
			v[i]:=nil;
	end;
	
	//PROCESO 1.B
	procedure leerPelicula(var p:pelicula);
	begin
		with p do begin
			write('Codigo de pelicula: '); readln(codPelicula);
			if(codPelicula <> -1)then begin
				write('Codigo de genero: '); readln(codGenero);
				write('Puntaje otorgado: '); readln(puntaje);
				writeln();
			end;
		end;
	end;
	
	//PROCESO 1.C 
	procedure agregarOrdenado(var l:lista; p:pelicula);
	var
		ant,act,nue:lista;
	begin
		new(nue);
		nue^.dato:=p;
		ant:=l; act:=l;
		
		while((act <> nil)AND(act^.dato.codPelicula < p.codPelicula))do begin
			ant:=act;
			act:=act^.sig;
		end;
		
		if(ant = act)
			then l:=nue
			else ant^.sig:=nue;
		nue^.sig:=act;
	end;
	
	//PROCESO 1
	procedure cargarVector(var v:vectorPeliculas);
	var
		p:pelicula;
		pos:integer;
	begin
		pos:=1;
		inicializarVector(v); //Inicializamos cada componente del vector (que es una lista) en nil PROCESO 1.A
		
		writeln('Pelicula nro ',pos,': ');
		leerPelicula(p); //PROCESO 1.B
		while(p.codPelicula <> -1)do begin
			agregarOrdenado(v[p.codGenero],p); //PROCESO 1.C
			pos:=pos + 1;
			writeln('Pelicula nro ',pos,': ');
			leerPelicula(p); 
		end;
	end;
	
	//PROCESO 2.A
	procedure buscarMinimo(var v:vectorPeliculas; var min:pelicula);
	var  
		indiceMin,i:integer;
	begin
		indiceMin:=0;
		min.codPelicula:=9999;
		for i:= 1 to maxGenero do begin //MIRO EN TODAS LAS POSICION PRIMERAS DE LA LISTA
			if(v[i] <> nil)then //Evaluo que no sea nil
				if(v[i]^.dato.codPelicula <= min.codPelicula)then begin //veo cual es el minimo y actualizo
					indiceMin:=i;
					min:=v[i]^.dato;
				end;
		end;
		if(min.codPelicula <> 9999)then begin
			v[indiceMin]:=v[indiceMin]^.sig;
		end;
	end;
	
	//PROCESO 2.B
	procedure agregarAtras(var l,ult:lista; p:pelicula);
	var
		nue:lista;
	begin
		new(nue);
		nue^.dato:=p;
		nue^.sig:=nil;
		if(l = nil)
			then l:=nue
			else ult^.sig:=nue;
		ult:=nue;
	end;
	
	//PROCESO 2
	procedure mergePeliculas(v:vectorPeliculas; var l:lista);
	var
		min:pelicula;
		ult:lista;
	begin
		l:=nil;
		buscarMinimo(v,min); //BUSCAMOS EL MINIMO EN NUESTRAS LISTAS DE LA PRIMER POSICION  - PROCESO 2.A
		while(min.codPelicula <> 9999)do begin 
			agregarAtras(l,ult,min); //AGREGAMOS ATRAS LA PELICULA MINIMA - PROCESO 2.B
			buscarMinimo(v,min);
		end;
	end;
	
	//PROCESO 3.A
	procedure mostrarDatos(p:pelicula);
	begin
		writeln(' -Codigo de pelicula: ',p.codPelicula);
		writeln(' -Codigo de genero: ',p.codGenero);
		writeln(' -Puntaje Otrogado: ',p.puntaje);
		writeln();
	end;
	
	//PROCESO 3
	procedure mostrarMerge(l:lista);
	begin
		if(l <> nil)then begin
			mostrarDatos(l^.dato); //PROCESO 3.A
			mostrarMerge(l^.sig);
		end;
	end;
	
var
	v:vectorPeliculas;
	nueLista:lista;
begin
	cargarVector(v); //PROCESO 1
	mergePeliculas(v,nueLista); //PROCESO 2
	mostrarMerge(nueLista); //PROCESO 3
end.

{
 b)
 c)con merge 
 d)
 e)merge acumualdor ,retorna una lista con un isbn y cant listas,con nueva lista
 f)
}

program merge;
const
  maxMes = 4;
type
	rangoMes=1..maxMes;
	rangoDia=1..31;
	prestamo=record
      ISBN:integer;
      nroSocio:integer;
      dia:rangoDia;
      mes:rangoMes;
      cantPrestamo:integer;
	end;
 
	lista =^nodo;
	nodo=record
		dato:prestamo;
		sig:lista;
	end;
	
	infoPrestamo = record
		ISBN:integer;
		cantidad:integer;
	end;
	
	lista2 = ^nodo2;
	nodo2 = record
		dato:infoPrestamo;
		sig:lista2;
	end;
 
	vector = array[rangoMes]of lista;
	
	//PROCESO 1.A
	procedure inicializarVector(var v:vector);
	var
		i:rangoMes;
	begin
		for i:= 1 to maxMes do 
			v[i]:=nil;
	end;
	
	//PROCESO 1.B
	procedure leerPrestamo(var p:prestamo);
	begin
		write(' -ISBN del libro: '); readln(p.ISBN);
		if(p.ISBN <> -1) then begin
			write(' -Nro de socio: '); readln(p.nroSocio);
			write(' -Dia: '); readln(p.dia);
			write(' -Mes: '); readln(p.mes);
			write(' -Cantidad de dias prestado: '); readln(p.cantPrestamo);
			writeln();
		end
		else begin 
			writeln(' --- HEMOS FINALIZO CON LA CARGA ---');
			writeln();
		end;
	end;
	
	//PROCESO 1.C
	procedure agregarOrdenado(var l:lista; p:prestamo);
	var
		nue,act,ant:lista;
	begin
		new(nue);
		nue^.dato:=p;
		act:=l;
		ant:=l;

		while((act<>nil)and(act^.dato.ISBN< p.ISBN)) do begin
			ant:=act;
			act:=act^.sig;
		end;
		
		if(act=ant) 
			then l:=nue
			else ant^.sig:=nue;
			
		nue^.sig:=act;
	end;
 
	//PROCESO 1
	procedure cargarVector(var v:vector);
	var
		p:prestamo;
	begin
		inicializarVector(v); //PROCESO 1.A
		leerPrestamo(p); //PROCESO 1.B
		while(p.ISBN <> -1)do begin
			agregarOrdenado(v[p.mes], p); //PROCESO 1.C
			leerPrestamo(p);
		end;
	end;
	
	//PROCESO 2.A - Recursion
	procedure mostrarLista(l:lista);
	begin
		if(l <> nil)then begin 
			writeln(' ISBN: ',l^.dato.ISBN,'  - Dias prestado: ',l^.dato.cantPrestamo);
			writeln();
			mostrarLista(l^.sig);
		end
	end;

	//PROCESO 2
	procedure imprimirVector(v:vector);
	var
		i:rangoMes;
	begin
		for i:= 1 to maxMes do begin
			if(v[i] <> nil)then begin
				writeln(' ----- Lista del mes ',i, '------ ');
				mostrarLista(v[i]) //PROCESO 2.A
			end
			else begin
				writeln();
				writeln(' - Lista del mes ',i,' VACIA.....');
				writeln();
			end;
		end;
	end;
	
	//PROCESO 3.A
	procedure buscarMinimo(var v:vector; var min:prestamo);
	var  
		indiceMin,i:integer;
	begin
		indiceMin:=0;
		min.ISBN:=9999;
		for i:= 1 to maxMes do begin
			if(v[i] <> nil)then
				if(v[i]^.dato.ISBN <= min.ISBN)then begin
					indiceMin:=i;
					min:=v[i]^.dato;
				end;
		end;
		if(min.ISBN <> 9999)then begin
			v[indiceMin]:=v[indiceMin]^.sig;
		end;
	end;
	
	//PROCESO 3.B
	procedure agregarAtras(var l,ult:lista; dato:prestamo);
	var
		nue:lista;
	begin
		new(nue);
		nue^.dato:=dato;
		nue^.sig:=nil;
		if(l = nil)then
			l:=nue
		else
			ult^.sig:=nue;
		ult:=nue;
	end;
	
	//PROCESO 3
	procedure merge(v:vector; var nueLista:lista);
	var
		min:prestamo;
		ult:lista;
	begin
		nueLista:=nil; //inicializo la nueva lista
		buscarMinimo(v,min); //PROCESO 3.A
		while(min.ISBN <> 9999)do begin
			agregarAtras(nueLista,ult,min); //PROCESO 3.B
			buscarMinimo(v,min);  
		end;
	end;
	
	//PROCESO 4
	procedure imprimirMerge(l:lista);
	begin
		if(l <> nil)then begin
			writeln('ISBN: ',l^.dato.ISBN,' | Nro de socio: ',l^.dato.nroSocio);
			imprimirMerge(l^.sig);
		end;
	end;	
	
	//PROCESO 5.A
	procedure buscarMinimo2(var v:vector; var min:infoPrestamo);
	var  
		indiceMin,i:integer;
	begin
		indiceMin:=0;
		min.ISBN:=9999;
		for i:= 1 to maxMes do begin
			if(v[i] <> nil)then begin
				if(v[i]^.dato.ISBN <= min.ISBN)then begin
					indiceMin:=i;
					min.ISBN:=v[i]^.dato.ISBN;
				end;
			end;
		end;
		if(min.ISBN <> 9999)then begin
			v[indiceMin]:=v[indiceMin]^.sig;
		end;
	end;
	
	//PROCESO 5.B
	procedure agregarAtras2(var l,ult:lista2; dato:infoPrestamo);
	var
		nue:lista2;
	begin
		new(nue);
		nue^.dato:=dato;
		nue^.sig:=nil;
		if(l = nil)
			then l:=nue
			else ult^.sig:=nue;
		ult:=nue;
	end;
	
	//PROCESO 5
	procedure mergeAcumulador(v:vector; var l:lista2);
	var
		contador:integer;
		minPrestamo:infoPrestamo;
		actualPrestamo:infoPrestamo;
		ult:lista2;
	begin
		l:=nil; //PRIMERO INICIALIZAMOS LA NUESTA LISTA EN NIL
		buscarMinimo2(v,minPrestamo); //PROCESO 5.A
		while(minPrestamo.ISBN <> 9999)do begin
			actualPrestamo:=minPrestamo;
			contador:=0;
			while((minPrestamo.ISBN <> 9999) AND (actualPrestamo.ISBN = minPrestamo.ISBN))do begin
				contador:=contador + 1;
				buscarMinimo2(v,minPrestamo);
			end;
			actualPrestamo.cantidad:=contador;
			agregarAtras2(l,ult,actualPrestamo); //PROCESO 5.B
		end;
	end;
	
	//PROCESO 6
	procedure imprimirMergeAcumulador(l:lista2);
	begin
		if(l <> nil)then begin
			writeln('ISBN: ',l^.dato.ISBN,' | Cantidad total de prestamo: ',l^.dato.cantidad);
			imprimirMergeAcumulador(l^.sig);
		end;
	end;
		
var
   v:vector;
   nueLista:lista;
   listaAcumuladora:lista2;
begin
	cargarVector(v); //PROCESO 1
	imprimirVector(v); //PROCESO 2
	merge(v,nueLista); //PROCESO 3
	writeln('Lista generada con el metodo MERGE: ');
	imprimirMerge(nueLista); //PROCESO 4
	mergeAcumulador(v,listaAcumuladora); //PROCESO 5
	writeln('Merge acumulador: ');
	imprimirMergeAcumulador(listaAcumuladora); //PROCESO 6
end.



# Taller de Programacion

Materia de la Facultad de Informatica UNLP 1er Año

**Student:** Yamil Daza 👋

**LinkedIn:** https://www.linkedin.com/in/yamil-daza/ 🚀

## Ejercicios de "Taller de Programacion" - Practicas 🛠

### PARTE 1 - Imperativo:
   - Practica 1 -> [LINK](./Imperativo/Practica1/Imperativo---Prctica-Clase-1---Ordenacin.docx)
   - Practica 2 -> [LINK](./Imperativo/Practica2/Imperativo---Prctica-Clase-2---Recursin.docx)
   - Practica 3 -> [LINK](./Imperativo/Practica3/Imperativo---Prctica-Clase-3---rboles.docx)
   - Practica 4 -> [LINK](./Imperativo/Practica4/Imperativo---Prctica-4---Merge.docx)

### PARTE 2 - Programacion Orientado a Objetos: 
   - Practica 1 -> [LINK](./POO/Practica1/Practica-Tema-1-2022---SS.pdf)
   - Practica 2 -> [LINK](./POO/Practica2/Practica-Tema-2-2022---SS.pdf)
   - Practica 3 -> [LINK](./POO/Practica3/Practica-Tema-3-2022---SS.pdf)
   - Practica 4 -> [LINK](./POO/Practica4/Practica-Tema-4-2022---SS.pdf)
   - Repaso Final POO -> [LINK](./POO/RepasoFinal/Repaso---Mdulo-POO.pdf)

### PARTE 3 - Concurrencia 
   - Practica 1 -> [LINK](./Concurrencia/Practica1/Practica1-Concurrente.pdf)
   - Practica 2 -> [LINK](./Concurrencia/Practica2/Practica-2-Concurrente.pdf)
   - Practica 3 -> [LINK](./Concurrencia/Practica3/Practica-3-Concurrente.pdf)
   - Practica 4 -> [LINK](./Concurrencia/Practica4/Prctica-4-Concurrente.pdf)
